﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Utils
{
    public static class MomoUtils
    {
        public static string PayMomo(string orderID, string totalCost)
        {
            string endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor";
            string partnerCode = "MOMOJIGZ20211121";
            string accessKey = "bnlgOr3B6oghDWKq";
            string serectkey = "Pok73Uv4JeB3MPm2qTJBrX14T7zw8ybu";
            string orderInfo = "Thanh toan momo";
            string redirectUrl = "http://localhost:8080";//"http://trongdong.somee.com";
            //string ipnUrl = "http://localhost:8080";
            string requestType = "captureMoMoWallet";

            string amount = totalCost;
            string orderId = orderID;
            string requestId = Guid.NewGuid().ToString();
            string extraData = "merchantName=tmdt;merchantId=tmdt";

            //Before sign HMAC SHA256 signature
            ;
            string raw = "partnerCode=" + partnerCode +
            "&accessKey=" + accessKey +
            "&requestId=" + requestId +
            "&amount=" + amount +
            "&orderId=" + orderId +
            "&orderInfo=" + orderInfo +
            "&returnUrl=" + redirectUrl +
            "&notifyUrl=" + redirectUrl +
            "&extraData=" + extraData;

            MoMoSecurity crypto = new MoMoSecurity();
            //sign signature SHA256
            string signature = crypto.signSHA256(raw, serectkey);

            //build body json request
            JObject message = new JObject
            {
                { "partnerCode", partnerCode },
                { "accessKey", accessKey},
                { "requestId", requestId },
                { "amount", amount },
                { "orderId", orderId },
                { "orderInfo", orderInfo },
                { "redirectUrl", redirectUrl },
                { "returnUrl", redirectUrl},
                { "notifyUrl", redirectUrl },
                { "extraData", extraData },
                { "requestType", requestType },
                { "signature", signature }

            };
            string responseFromMomo = PaymentRequest.sendPaymentRequest(endpoint, message.ToString());

            return responseFromMomo;


        }
    }
}
