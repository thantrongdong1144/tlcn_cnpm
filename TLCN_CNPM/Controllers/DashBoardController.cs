﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Service;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashBoardController : BaseController<IDashBoardService>
    {
        public DashBoardController(IDashBoardService service) : base(service)
        {

        }
        [HttpGet("chart/revenue")]
        public async Task<ActionResult> GetRevenue()
        {
            try
            {
                var result = await _service.GetRevenueOrderByMonth();
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("chart/rejecttotal")]
        public async Task<ActionResult> GetRejecTotal()
        {
            try
            {
                var result = await _service.GetRejectTotalOrderByMonth();
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("chart/num_order_in_month")]
        public async Task<ActionResult> GetNumOfOrderByMonth()
        {
            try
            {
                var result = await _service.GetNumOfOrderByMonth();
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("chart/total_product_order_by_category")]
        public async Task<ActionResult> GetProductTotalOrderByCategory()
        {
            try
            {
                var result = await _service.GetProductTotalOrderByCategory();
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("parameter/total_order")]
        public async Task<ActionResult> GetParameter()
        {
            try
            {
                var resultTotalOrder = await _service.TotalNumberOfSuccessOrder();
                return Ok(new
                {
                    data = new {
                        total_order = resultTotalOrder,
                    },
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("parameter/total_revenue")]
        public async Task<ActionResult> GetParameterTotalRevenue()
        {
            try
            {
                var resultTotalRevenue = await _service.TotalRevenue();
                return Ok(new
                {
                    data = new
                    {
                        total_revenue = resultTotalRevenue,
                    },
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("parameter/total_user")]
        public async Task<ActionResult> GetParameterTotalUser()
        {
            try
            {
                var resultTotalUser = await _service.TotalUser();
                return Ok(new
                {
                    data = new
                    {
                        total_user = resultTotalUser,
                    },
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("parameter/total_reject_order")]
        public async Task<ActionResult> GetParameterTotalRejectOrder()
        {
            try
            {
                var resultTotalRejectOrder = await _service.TotalRejectOrder();
                return Ok(new
                {
                    data = new
                    {
                        total_reject_order = resultTotalRejectOrder
                    },
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
    }
}
