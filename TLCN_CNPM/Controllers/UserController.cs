﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Service;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        //private readonly MarketDBContext _context;
        public IUserService _service;
        public IOrderService _orderService;

        public UserController(IUserService service, IOrderService orderService)
        {
            _service = service;
            _orderService = orderService;
        }
        // GET: api/user/1
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(int id)
        {
            var user = await _service.GetByIdAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        //GET: api/user
        //[HttpGet]
        //[Route("")]
        //public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        //{
        //    return await _service.GetAllAsync();
        //}


        [HttpGet]
        public async Task<ActionResult> GetUsers([FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetAsync(validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _service.CountAsync();
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("shipper")]
        public async Task<ActionResult> GetShippers([FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetShipperAsync(validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _service.CountShipperAsync();

                var dataReturn = result.Select(el => new
                {
                    Id = el.Id,
                    FirstName = el.FirstName,
                    LastName = el.LastName,
                    Avatar = el.Avatar,
                    ChargeArea = el.ChargeArea,
                    NumOfOrderShipping = _orderService.CountAsyncByStatusAndShipperId(OrderStatus.Delivering, el.Id)
                });
                return Ok(new
                {
                    data = dataReturn,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpGet("by-admin")]
        public async Task<ActionResult> GetUsersByAdmin([FromQuery] PaginationFilter filter, [FromQuery] string search)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetAsyncByAdmin(validFilter.PageNumber, validFilter.PageSize, search);
                var totalElements = await _service.CountAsyncByAdmin(search);
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddUserAsync([FromBody] AddUserHasUserrleModelView viewModel)
        {
            try
            {
                User user = new User
                {
                    FirstName = viewModel.FirstName,
                    LastName = viewModel.LastName,
                    Username = viewModel.Username,
                    Phone = viewModel.Phone,
                    Avatar = "https://res.cloudinary.com/donghcmute/image/upload/v1634690680/user_bbt8lk.png",
                    Sex = viewModel.Sex,
                    Email = viewModel.Email,
                    UserRole = (UserRole)viewModel.UserRole,
                    Status = viewModel.Status,
                    PassWord = viewModel.PassWord,
                    RegisterDate = new DateTime(),
                    ChargeArea = viewModel.ChargeArea,
                    
                };
                var rs = await _service.AddAsync(user);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Add fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Add success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateUserAsync(int id, [FromBody] UpdateUserModelView viewModel)
        {
            try
            {
                User user = await _service.GetByIdAsync(id);
                if (user == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "user doesn't exist"
                    });
                }
                user.FirstName = viewModel.FirstName;
                user.LastName = viewModel.LastName;
                user.Username = viewModel.Username;
                user.Phone = viewModel.Phone;
                user.Avatar = viewModel.Avatar;
                user.Sex = viewModel.Sex;
                user.Email = viewModel.Email;
                user.UserRole = (UserRole)viewModel.UserRole;
                user.Status = viewModel.Status;
                user.PassWord = viewModel.PassWord;
                var rs = await _service.UpdateAsync(user);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var user = await _service.GetByIdAsync(id);
                if (user == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "User doesn't exist"
                    });
                }
                var rs = await _service.DeleteAsync(user);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        // PUT: api/Users/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutUser(string id, User user)
        //{
        //    if (id != user.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(user).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!UserExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Users
        //[HttpPost]
        //public async Task<ActionResult<User>> PostUser(User user)
        //{
        //    var rs = await _context.Users.AddAsync(user);
        //    try
        //    {
        //         await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (UserExists(user.Id))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return CreatedAtAction("GetUser", new { id = user.Id }, user);
        //}

        //// DELETE: api/Users/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteUser(string id)
        //{
        //    var user = await _context.Users.FindAsync(id);
        //    if (user == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Users.Remove(user);
        //    await _context.SaveChangesAsync();

        //    return NoContent();
        //}

        //private bool UserExists(string id)
        //{
        //    return _context.Users.Any(e => e.Id == id);
        //}
    }
}
