﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Service;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;
using Microsoft.AspNetCore.Authorization;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : BaseController<ICommentService>
    {
        public CommentController(ICommentService service) : base(service)
        {

        }
        
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> GetComments([FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetAsync(validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _service.CountAsync();
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        // GET: api/comment/1
        [HttpGet("{id}")]
        public async Task<ActionResult<Comment>> GetComment(int id)
        {
            var cmt = await _service.GetByIdAsync(id);

            if (cmt == null)
            {
                return NotFound();
            }

            return Ok(cmt);
        }
        [HttpGet("product/{id}")]
        public async Task<ActionResult> getCommentByProductId(int id, [FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetCommentByProductId(id, validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _service.CoutByProductIdAsync(id);
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPost]
        public async Task<ActionResult> AddComment([FromBody] AddCommentViewModel commentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            Comment comment = new Comment
            {
                Star = commentModel.Star,
                CommentDate = commentModel.CommentDate,
                CommentContent = commentModel.CommentContent,
                UserId = commentModel.UserId,
                ProductId = commentModel.ProductId
            };
            CommentResponseModel rs = await _service.AddComment(comment);
            if (rs.IsSuccess)
            {
                return Ok(new
                {
                    code = 200,
                    message = "Add Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Add Fail!"
                });
            }
        }

        //[HttpPost("admin")]
        //public async Task<ActionResult> AddCommentAsync([FromBody] AddCommentViewModel viewModel)
        //{
        //    try
        //    {
        //        Comment cmt = new Comment
        //        {
        //            Star = viewModel.Star,
        //            CommentDate = new DateTime(),
        //            CommentContent = viewModel.CommentContent,
        //            UserId = viewModel.UserId,
        //            ProductId = viewModel.ProductId
        //        };
        //        var rs = await _service.AddComment(cmt);
        //        if (!rs.IsSuccess)
        //        {
        //            return Ok(new
        //            {
        //                code = 400,
        //                message = "Add fail"
        //            });
        //        }

        //        return Ok(new
        //        {
        //            code = 200,
        //            message = "Add success"
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(new
        //        {
        //            code = 400,
        //            message = ex.Message
        //        });
        //    }
        //}
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateCommentAsync(int id, [FromBody] UpdateCommentModelView viewModel)
        {
            try
            {
                Comment cmt = await _service.GetByIdAsync(id);
                if (cmt == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "comment doesn't exist"
                    });
                }
                cmt.Star = viewModel.Star;
                //cmt.CommentDate = viewModel.CommentDate;
                cmt.CommentContent = viewModel.CommentContent;
                cmt.UserId = viewModel.UserId;
                cmt.ProductId = viewModel.ProductId;
     
                var rs = await _service.UpdateAsync(cmt);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var cmt = await _service.GetByIdAsync(id);
                if (cmt == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Comment doesn't exist"
                    });
                }
                var rs = await _service.DeleteAsync(cmt);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
    }
}
