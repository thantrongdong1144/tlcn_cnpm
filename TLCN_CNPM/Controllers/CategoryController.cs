﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Service;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : BaseController<ICategoryService>
    {
        public CategoryController(ICategoryService service) : base(service)
        {

        }
        [HttpGet]
        public async Task<ActionResult> GetCategories([FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetAsync(validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _service.CountAsync();
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("by-admin")]
        public async Task<ActionResult> GetCategoriesByAdmin([FromQuery] PaginationFilter filter, [FromQuery] string search)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetAsyncByAdmin(validFilter.PageNumber, validFilter.PageSize, search);
                var totalElements = await _service.CountAsyncByAdmin(search);
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("withquantity")]
        public async Task<ActionResult> GetCategoriesWithQuantity()
        {
            try
            {
                var result = await _service.GetCategoryWithTotalProduct();
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        // GET: api/category/1
        [HttpGet("{id}")]
        public async Task<ActionResult> GetCategory(int id)
        {
            try
            {
                var products = await _service.GetByIdAsync(id);

                if (products == null)
                {
                    return NotFound();
                }

                return Ok(new
                {
                    data = products,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPost]
        public async Task<ActionResult> AddCategoryAsync([FromBody] AddCategoryModelView viewModel)
        {
            try
            {
                Category cate = new Category
                {
                    Name = viewModel.Name,
                    Icon = viewModel.Icon
                };
                var rs = await _service.AddAsync(cate);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Add fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Add success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateCategoryAsync(int id, [FromBody] UpdateCategoryModelView viewModel)
        {
            try
            {
                Category cate = await _service.GetByIdAsync(id);
                if(cate == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Category doesn't exist"
                    });
                }
                cate.Icon = viewModel.Icon;
                cate.Name = viewModel.Name;
                var rs = await _service.UpdateAsync(cate);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            try
            {
                var cate = await _service.GetByIdAsync(id);
                if (cate == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Category doesn't exist"
                    });
                }
                var rs = await _service.DeleteAsync(cate);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch(Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
    }
}
