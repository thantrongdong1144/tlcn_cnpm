﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Service;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;
//using XAct.Messages;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : BaseController<IProductService>
    {
        public ProductController(IProductService service) : base(service)
        {

        }
        [HttpGet]
        public async Task<ActionResult> GetProducts([FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetAsync(validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _service.CountAsync();
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("by-admin")]
        public async Task<ActionResult> GetProductsByAdmin([FromQuery] PaginationFilter filter, [FromQuery] string search)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetProductsByAdmin(validFilter.PageNumber, validFilter.PageSize, search);
                var totalElements = await _service.CountAsyncByAdmin(search);
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        // GET: api/product/1
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            try
            {
                var products = await _service.GetByIdAsync(id);

                if (products == null)
                {
                    return NotFound();
                }

                return Ok(new 
                { 
                    data = products,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        // GET: api/product/search/category/1
        [HttpGet("search/category/{id}")]
        public async Task<ActionResult<Product>> GetProductByCategory(int id, [FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetByCategoryIdAsync(id, validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _service.CountAsyncByCategoryId(id);
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("search/product/{key}")]
        public async Task<ActionResult<Product>> searchProductByKey(string key, [FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetByProductKeyAsync(key, validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _service.CountAsyncByProductKey(key);
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("popular")]
        public async Task<ActionResult> GetPopularProduct()
        {
            try
            {
                var result = await _service.GetPopularProduct();
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("new")]
        public async Task<ActionResult> GetNewProduct()
        {
            try
            {
                var result = await _service.GetNewProduct();
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("popularlastweek")]
        public async Task<ActionResult> GetPopularProductLastWeek()
        {
            try
            {
                var result = await _service.GetPopularProductLastWeek();
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPost]
        public async Task<ActionResult> AddProduct([FromBody] AddProductViewModel viewModel)
        {
            try
            {
                Product prod = new Product
                {
                    Code = viewModel.Code,
                    Name = viewModel.Name,
                    Picture = viewModel.Picture,
                    Description = viewModel.Description,
                    Price = viewModel.Price,
                    Quantity = viewModel.Quantity,
                    Like = viewModel.Like,
                    Star = viewModel.Star,
                    CategoryId = viewModel.CategoryId,
                    CreatedDate = new DateTime(),
                    Unit = "kg"
                };
                var rs = await _service.AddProduct(prod);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Add fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Add success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateProductAsync(int id, [FromBody] UpdateProductViewModel viewModel)
        {
            try
            {
                Product prod = await _service.GetByIdAsync(id);
                if (prod == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "comment doesn't exist"
                    });
                }
                prod.Code = viewModel.Code;
                prod.Name = viewModel.Name;
                prod.Picture = viewModel.Picture;
                prod.Description = viewModel.Description;
                prod.Price = viewModel.Price;
                prod.Quantity = viewModel.Quantity;
                prod.Like = viewModel.Like;
                prod.Star = viewModel.Star;
                prod.CategoryId = viewModel.CategoryId;


                var rs = await _service.UpdateAsync(prod);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpGet("hasDiscount")]
        public async Task<ActionResult> GetProductsHasDiscount([FromQuery] PaginationFilter filter, [FromQuery] int isDiscount)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetProductsByDiscount(validFilter.PageNumber, validFilter.PageSize, isDiscount);
                var totalElements = await _service.CountAsyncByDiscount(isDiscount);
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpPost("discount")]
        public async Task<ActionResult> UpdateProductDiscount([FromBody] UpdateDiscountProductViewModel viewModel)
        {
            try
            {
                foreach(var data in viewModel.ListDiscount)
                {
                    Product prod = await _service.GetByIdAsync(data.Id);
                    if (prod == null)
                    {
                        return Ok(new
                        {
                            code = 400,
                            message = "Product doesn't exist"
                        });
                    }
                    prod.Price = data.NewPrice;
                    prod.DiscountPrice = data.DiscountPrice;
                    prod.ExpirationDiscountDate = data.ExpirationDiscountDate;


                    var rs = await _service.UpdateAsync(prod);
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            try
            {
                var prod = await _service.GetByIdAsync(id);
                if (prod == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Product doesn't exist"
                    });
                }
                var rs = await _service.DeleteAsync(prod);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("check-on-sale/{id}")]
        public async Task<ActionResult> CheckOnSale(int id)
        {
            try
            {
                var result = await _service.GetByIdAsync(id);

                return Ok(new
                {
                    data = new {
                        isDeleted = result.RecordStatus == EntityStatus.Deleted || result.Category.RecordStatus == EntityStatus.Deleted,
                        data = result
                    },
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
    }
}
