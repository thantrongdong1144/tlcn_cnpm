﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Service;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavoriteProductController : BaseController<IFavoriteProductService>
    {
        public FavoriteProductController(IFavoriteProductService service) : base(service)
        {

        }
        [HttpGet("user/{id}")]
        public async Task<ActionResult> GetListfavoiteProducts(int id)
        {
            try
            {
                var result = await _service.GetListFavoriteProductByUserId(id);
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("{userId}/{productId}")]
        public async Task<ActionResult> GetFavoriteProducts(int userId, int productId)
        {
            try
            {
                var result = await _service.GetFavoriteProduct(userId, productId);
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPost("like/{userId}/{productId}")]
        public async Task<ActionResult> LikeOrUnlikeProduct(int userId, int productId)
        {
            //Like if it doesn't exist in FavoriteProduct table
            //Unlike if it exist in FavoriteProduct table
            try
            {
                FavoriteProduct product = await _service.GetFavoriteProduct(userId, productId);
                if(product == null)
                {
                    //Like
                    FavoriteProduct prod = new FavoriteProduct
                    {
                        ProductId = productId,
                        UserId = userId
                    };
                    var rs = await _service.AddAsync(prod);
                    if (!rs.IsSuccess)
                    {
                        return Ok(new
                        {
                            message = "Like fail",
                            code = 400
                        });
                    }
                    return Ok(new
                    {
                        message = "Like success",
                        code = 200
                    });
                } else
                {
                    //Unlike
                    var rs = await _service.DeleteAsync(product);
                    if (!rs.IsSuccess)
                    {
                        return Ok(new
                        {
                            message = "Unlike fail",
                            code = 400
                        });
                    }
                    return Ok(new
                    {
                        message = "Unlike success",
                        code = 200
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
    }
}
