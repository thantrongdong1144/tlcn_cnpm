﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Service;
using TLCN_CNPM.Model;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Utils;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        public readonly IAuthService _service;
        private readonly AppSettings _appSettings;
        private readonly IMailService _mailService;
        public IConfiguration _config;
        public AuthController(IAuthService service, IOptions<AppSettings> appSettings, IMailService mailService, IConfiguration config)
        {
            _service = service;
            _appSettings = appSettings.Value;
            _mailService = mailService;
            _config = config;
        }
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync(LoginViewModel loginView)
        {
            try
            {
                var result = await _service.LoginAsync(loginView);
                if (result.IsSuccess)
                {
                    var token = Authenticate.GenerateJSONWebToken(result.User, _config);
                    return Ok(new
                    {
                        code = 200,
                        data = result.User,
                        token = token,
                        message = "Login success"
                    });
                }
                else
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Login fail"
                    });
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [AllowAnonymous]
        [HttpPost("forgotpassword")]
        public async Task<IActionResult> ForgotPasswordAsync([FromBody] ForgotPasswordViewModel viewModel)
        {
            try
            {
                User user = await _service.GetByEmailAsync(viewModel.Email);
                if(user == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Invalid email"
                    });
                }
                string _code = StringUtil.RandomString(12, false);
                user.RefreshCode = _code;
                var rs = await _service.UpdateUser(user);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }
                var mailData = new MailRequestModel
                {
                    ToEmail = viewModel.Email,
                    Subject = "Đổi mật khẩu của bạn",
                    Body = $"Vui lòng nhấn vào link sau để đặt lại mật khẩu " +
                    $"{_appSettings.BaseUrl}/confirmpassword?code={_code}"
                };
                await _mailService.SendEmailAsync(mailData);
                var baseUrl = _appSettings.BaseUrl;
                return Ok(new
                {
                    code = 200,
                    message = "Send success"
                });

            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] AddUserViewModel viewModel)
        {
            try
            {
                User user = await _service.GetByEmailAsync(viewModel.Email);
                if (user != null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Email already exists"
                    });
                }
                User newUser = new User
                {
                    FirstName = viewModel.FirstName,
                    LastName = viewModel.LastName,
                    Username = viewModel.Username,
                    Phone = viewModel.Phone,
                    Sex = viewModel.Sex,
                    Email = viewModel.Email,
                    PassWord = viewModel.PassWord,
                    Status = true,
                    Avatar = "https://res.cloudinary.com/donghcmute/image/upload/v1634690680/user_bbt8lk.png",
                    RegisterDate = new DateTime(),
                    UserRole = UserRole.Customer
                };
                var rs = await _service.AddUserAsync(newUser);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Add fail"
                    });
                }
                var mailData = new MailRequestModel
                {
                    ToEmail = viewModel.Email,
                    Subject = "Đăng ký tài khoản thành công",
                    Body = $"Bạn đã đăng ký tài khoản tại Market "
                };
                await _mailService.SendEmailAsync(mailData);
                return Ok(new
                {
                    code = 200,
                    message = "Send success"
                });

            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPost("confirmpassword")]
        public async Task<IActionResult> ConfirmPasswordAsync([FromBody] ConfirmPasswordViewModel viewModel)
        {
            try
            {
                User user = await _service.GetByEmailAsync(viewModel.Email);
                if (user == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Invalid email"
                    });
                }
                if(viewModel.Code != user.RefreshCode)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Invalid code"
                    });
                }
                user.PassWord = viewModel.NewPassword;
                var rs = await _service.UpdateUser(user);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });

            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPut("user/update/{id}")]
        public async Task<IActionResult> UpdateUser(int id, [FromBody] UpdateUserViewModel userModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Some properties are not valid");
                }
                User user = await _service.GetByIdAsync(id);
                if (user == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "No valid user"
                    });
                }
                user.Avatar = userModel.Avatar;
                user.Sex = userModel.Sex;
                user.FirstName = userModel.FirstName;
                user.LastName = userModel.LastName;
                user.Phone = userModel.Phone;
                var result = await _service.UpdateUser(user);
                if (result.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 200,
                        data = result.User,
                        message = "Update success"
                    });
                }
                else
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPut("user/changePassword/{id}")]
        public async Task<IActionResult> ChangePassWord(int id, [FromBody] ChangePasswordViewModel userModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Some properties are not valid");
                }
                User user = await _service.GetByIdAsync(id);
                if (user == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "No valid user"
                    });
                }
                if (user.PassWord != userModel.OldPassword)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Password is invalid"
                    });
                }
                user.PassWord = userModel.NewPassWord;
                var result = await _service.UpdateUser(user);
                if (result.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 200,
                        data = result.User,
                        message = "Update success"
                    });
                }
                else
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

    }
}
