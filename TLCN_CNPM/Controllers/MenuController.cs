﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Service;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private IMenuService _menuService;
        private IMenuDetailService _menuDetailService;
        public MenuController(IMenuService MenuService, IMenuDetailService menuDetailService)
        {
            _menuService = MenuService;
            _menuDetailService = menuDetailService;
        }

        [HttpGet]
        public async Task<ActionResult> GetOrder([FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _menuService.GetAsync(validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _menuService.CountAsync();
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> PostMenu([FromBody] AddMenuViewModel menuData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            Menu menu = new Menu
            {
                MenuName = menuData.MenuName,
            };
            MenuRepositoryModel rs = await _menuService.AddMenu(menu);
            if (rs.IsSuccess)
            {
                try
                {
                    if (menuData.MenuDetails.Count > 0 && rs.Menu.Id != null)
                    {
                        foreach (AddMenuDetailViewModel data in menuData.MenuDetails)
                        {
                            MenuDetail detail = new MenuDetail
                            {
                                ProductId = data.ProductId,
                                TotalKalo = data.TotalKalo,
                                MenuId = rs.Menu.Id
                            };
                            await _menuDetailService.AddMenuDetail(detail);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = ex.Message
                    });
                }
                return Ok(new
                {
                    code = 200,
                    message = "Add Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Add Fail!"
                });
            }
        }


        [HttpPost("byadmin")]
        public async Task<ActionResult> PostMenuAdmin([FromBody] AddMenuByAdminViewModel menuData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            Menu menu = new Menu
            {
                MenuName = menuData.MenuName
            };
            var sum = 0;
            foreach (AddMenuDetailViewModel detail in menuData.MenuDetails)
            {
                sum += detail.TotalKalo;
            }
            menu.RealTotalKalo = sum;
            var rs = await _menuService.AddMenu(menu);
            if (rs.IsSuccess)
            {
                try
                {
                    if (menuData.MenuDetails.Count > 0)
                    {
                        foreach (AddMenuDetailViewModel data in menuData.MenuDetails)
                        {
                            MenuDetail detail = new MenuDetail
                            {
                                ProductId = data.ProductId,
                                TotalKalo = data.TotalKalo,
                                MenuId = rs.Menu.Id
                            };
                            await _menuDetailService.AddMenuDetail(detail);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = ex.Message
                    });
                }
                return Ok(new
                {
                    code = 200,
                    message = "Add Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Add Fail!"
                });
            }
        }
        
        [HttpPut("update/{id}")]
        public async Task<IActionResult> UpdateMenu(int id, [FromBody] UpdateMenuViewModel menuData)
        {
            try
            {
                var menu = await _menuService.GetByIdAsync(id);
                if (menu == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Category doesn't exist"
                    });
                }
                //order.Status = OrderStatus.Approved;
                
                menu.RealTotalKalo = menuData.RealTotalKalo;
                menu.MenuName = menuData.MenuName;
                var rs = await _menuService.UpdateAsync(menu);
                if (!rs.IsSuccess)
                {
                    try
                    {
                        foreach (AddMenuDetailViewModel model in menuData.MenuDetails)
                        {
                            MenuDetail detail = await _menuDetailService.GetByMenuIdAndProductId(id, model.ProductId);
                            if (detail != null)
                            {
                                detail.TotalKalo = model.TotalKalo;
                                await _menuDetailService.UpdateAsync(detail);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        return Ok(new
                        {
                            code = 400,
                            message = ex.Message
                        });
                    }
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpPost("item")]
        public async Task<ActionResult> AddItemToMenu([FromBody] AddMenuDetailToMenuViewModel menuData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            MenuDetail menu = new MenuDetail
            {
                TotalKalo = menuData.TotalKalo,
                MenuId = menuData.MenuId,
                ProductId = menuData.ProductId,
            };
            var rs = await _menuDetailService.AddMenuDetail(menu);
            if (rs.IsSuccess)
            {
                return Ok(new
                {
                    code = 200,
                    message = "Add Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Add Fail!"
                });
            }
        }

        [HttpPut("item")]
        public async Task<ActionResult> UpdateItemToMenu([FromBody] UpdateMenuDetailInMenuViewModel menuData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            MenuDetail menu = await _menuDetailService.GetByMenuIdAndProductId(menuData.MenuId, menuData.ProductId);
            if (menu == null)
            {
                return Ok(new
                {
                    code = 400,
                    message = "OrderId or ProductID doesn't exist!"
                });
            }
            menu.TotalKalo = menuData.TotalKalo;
            
            var rs = await _menuDetailService.UpdateAsync(menu);
            if (rs.IsSuccess)
            {
                return Ok(new
                {
                    code = 200,
                    message = "Update Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Update Fail!"
                });
            }
        }


        [HttpDelete("item/{menuId}/{productId}")]
        public async Task<ActionResult> DeleteMenuDetail(int menuId, int productId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            MenuDetail menu = await _menuDetailService.GetByMenuIdAndProductId(menuId, productId);
            if (menu == null)
            {
                return Ok(new
                {
                    code = 400,
                    message = "OrderId or ProductID doesn't exist!"
                });
            }
            var rs = await _menuDetailService.DeleteAsync(menu);
            if (rs.IsSuccess)
            {
                return Ok(new
                {
                    code = 200,
                    message = "Delete Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Delete Fail!"
                });
            }
        }


        [HttpGet("items/{menuId}")]
        public async Task<ActionResult> GetListItemByMenuId(int menuId)
        {
            try
            {
                List<MenuDetail> menu = await _menuDetailService.GetListMenuDetailsByMenuId(menuId);
                if (menu == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "OrderId doesn't exist!"
                    });
                }
                return Ok(new
                {
                    code = 200,
                    message = "Get Success!",
                    data = menu
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
    }
}
