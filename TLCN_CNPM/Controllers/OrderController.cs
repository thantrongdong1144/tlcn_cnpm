﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Service;
using TLCN_CNPM.Model;
using TLCN_CNPM.Utils;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private IOrderService _orderService;
        private IOrderDetailService _orderDetailService;
        private IMailService _mailService;
        private IUserService _userService;
        public OrderController(IOrderService OrderService, IOrderDetailService orderDetailService, IMailService mailService, IUserService userService)
        {
            _orderService = OrderService;
            _orderDetailService = orderDetailService;
            _mailService = mailService;
            _userService = userService;
        }
        [HttpPost]
        public async Task<ActionResult> PostOrder([FromBody] AddOrderViewModel orderData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            Order order = new Order
            {
                ReceiverName = orderData.ReceiverName,
                ReceiverPhone = orderData.ReceiverPhone,
                ReceiverAddress = orderData.ReceiverAddress,
                Status = orderData.PaymentType == PaymentType.VNPay ? OrderStatus.NotPay : OrderStatus.Processing,
                OrderDate = orderData.OrderDate,
                Note = orderData.Note,
                UserId = orderData.UserId,
                RealTotalCost = orderData.RealTotalCost,
                IsApplyDiscount = orderData.IsApplyDiscount,
                PaymentType = orderData.PaymentType
            };
            OrderResponseModel rs = await _orderService.AddOrder(order);
            if (rs.IsSuccess)
            {
                try
                {
                    if (orderData.OrderDetails.Count > 0 && rs.Order.Id != null)
                    {
                        foreach (AddOrderDetailViewModel data in orderData.OrderDetails)
                        {
                            OrderDetail detail = new OrderDetail
                            {
                                ProductId = data.ProductId,
                                RealPrice = data.RealPrice,
                                Quantity = data.Quantity,
                                OrderId = rs.Order.Id
                            };
                            await _orderDetailService.AddOrderDetail(detail);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = ex.Message
                    });
                }
                User orderUser = await _userService.GetByIdAsync(orderData.UserId);
                if (orderUser != null && orderUser.Email != null)
                {
                    var mailData = new MailRequestModel
                    {
                        ToEmail = orderUser.Email,
                        Subject = "Bạn đã đặt một đơn hàng thành công từ Market",
                        Body = $"Bạn đã đặt hàng thành công, đơn hàng có mã số {rs.Order.Id}." +
                        $"Vui lòng theo dõi đơn hàng để nhận được những cập nhật mới nhất từ chúng tôi." +
                        $"Trân trọng cảm ơn bạn đã sử dụng dịch vụ của chúng tôi."
                    };
                    await _mailService.SendEmailAsync(mailData);
                }

                var returnMessage = "Add success!";
                if(orderData.PaymentType == PaymentType.VNPay)
                {
                    returnMessage = MomoUtils.PayMomo(order.Id.ToString(), order.RealTotalCost.ToString());//
                }

                return Ok(new
                {
                    code = 200,
                    message = returnMessage
                });
            } else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Add Fail!"
                });
            }
        }
        
        [HttpPost("byadmin")]
        public async Task<ActionResult> PostOrderAdmin([FromBody] AddOrderByAdminViewModel orderData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            Order order = new Order
            {
                ReceiverName = orderData.ReceiverName,
                ReceiverPhone = orderData.ReceiverPhone,
                ReceiverAddress = orderData.ReceiverAddress,
                Status = OrderStatus.Approved,
                OrderDate = orderData.OrderDate,
                Note = orderData.Note,
                UserId = orderData.UserId,
                IsApplyDiscount = false
            };
            var sum = 0;
            foreach (AddOrderDetailViewModel detail in orderData.OrderDetails)
            {
                sum += detail.Quantity * detail.RealPrice;
            }
            order.RealTotalCost = sum;
            var rs = await _orderService.AddOrder(order);
            if (rs.IsSuccess)
            {
                try
                {
                    if (orderData.OrderDetails.Count > 0)
                    {
                        foreach (AddOrderDetailViewModel data in orderData.OrderDetails)
                        {
                            OrderDetail detail = new OrderDetail
                            {
                                ProductId = data.ProductId,
                                RealPrice = data.RealPrice,
                                Quantity = data.Quantity,
                                OrderId = rs.Order.Id
                            };
                            await _orderDetailService.AddOrderDetail(detail);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = ex.Message
                    });
                }
                return Ok(new
                {
                    code = 200,
                    message = "Add Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Add Fail!"
                });
            }
        }
        
        [HttpGet]
        public async Task<ActionResult> GetOrder([FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _orderService.GetAsync(validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _orderService.CountAsync();
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpGet("by-admin")]
        public async Task<ActionResult> GetOrderByAdmin([FromQuery] PaginationFilter filter, [FromQuery] string search)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _orderService.GetAsyncByAdmin(validFilter.PageNumber, validFilter.PageSize, search);
                var totalElements = await _orderService.CountAsyncByAdmin(search);
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpGet("by-status")]
        public async Task<ActionResult> GetOrderByStatus([FromQuery] PaginationFilter filter, [FromQuery] OrderStatus status)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _orderService.GetAsyncByStatus(validFilter.PageNumber, validFilter.PageSize, status);
                var totalElements = await _orderService.CountAsyncByStatus(status);
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            try
            {
                var order = await _orderService.GetByIdAsync(id);
                if (order == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Category doesn't exist"
                    });
                }
                var rs = await _orderService.DeleteAsync(order);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        
        [HttpPut("approved/{id}")]
        public async Task<IActionResult> ApprovedOrrder(int id)
        {
            try
            {
                var order = await _orderService.GetByIdAsync(id);
                if (order == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Category doesn't exist"
                    });
                }
                order.Status = OrderStatus.Approved;
                var rs = await _orderService.UpdateAsync(order);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        
        [HttpPut("update/{id}")]
        public async Task<IActionResult> UpdateOrder(int id, [FromBody] UpdateOrderViewModel orderData)
        {
            try
            {
                var order = await _orderService.GetByIdAsync(id);
                if (order == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Category doesn't exist"
                    });
                }
                //order.Status = OrderStatus.Approved;
                order.ReceiverName = orderData.ReceiverName;
                order.ReceiverPhone = orderData.ReceiverPhone;
                order.ReceiverAddress = orderData.ReceiverAddress;
                order.Status = orderData.Status;
                order.Note = orderData.Note;
                orderData.RealTotalCost = orderData.RealTotalCost;
                var rs = await _orderService.UpdateAsync(order);
                if (!rs.IsSuccess)
                {
                    try
                    {
                        foreach (AddOrderDetailViewModel model in orderData.OrderDetails)
                        {
                            OrderDetail detail = await _orderDetailService.GetByOrderIdAndProductId(id, model.ProductId);
                            if (detail != null)
                            {
                                detail.Quantity = model.Quantity;
                                detail.RealPrice = model.RealPrice;
                                await _orderDetailService.UpdateAsync(detail);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        return Ok(new
                        {
                            code = 400,
                            message = ex.Message
                        });
                    }
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        
        [HttpPost("item")]
        public async Task<ActionResult> AddItemToOrder([FromBody] AddOrderDetailToOrderViewModel orderData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            OrderDetail order = new OrderDetail
            {
                OrderId = orderData.OrderId,
                ProductId = orderData.ProductId,
                RealPrice = orderData.RealPrice,
                Quantity = orderData.Quantity
            };
            var rs = await _orderDetailService.AddOrderDetail(order);
            if (rs.IsSuccess)
            {
                return Ok(new
                {
                    code = 200,
                    message = "Add Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Add Fail!"
                });
            }
        }
        
        [HttpPut("item")]
        public async Task<ActionResult> UpdateItemToOrder([FromBody] UpdateOrderDetailInOrderViewModel orderData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            OrderDetail order = await _orderDetailService.GetByOrderIdAndProductId(orderData.OrderId, orderData.ProductId);
            if (order == null)
            {
                return Ok(new
                {
                    code = 400,
                    message = "OrderId or ProductID doesn't exist!"
                });
            }
            order.Quantity = orderData.Quantity;
            order.RealPrice = orderData.RealPrice;
            var rs = await _orderDetailService.UpdateAsync(order);
            if (rs.IsSuccess)
            {
                return Ok(new
                {
                    code = 200,
                    message = "Update Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Update Fail!"
                });
            }
        }
        
        [HttpDelete("item/{orderId}/{productId}")]
        public async Task<ActionResult> DeleteOrderDetail(int orderId, int productId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            OrderDetail order = await _orderDetailService.GetByOrderIdAndProductId(orderId, productId);
            if (order == null)
            {
                return Ok(new
                {
                    code = 400,
                    message = "OrderId or ProductID doesn't exist!"
                });
            }
            var rs = await _orderDetailService.DeleteAsync(order);
            if (rs.IsSuccess)
            {
                return Ok(new
                {
                    code = 200,
                    message = "Delete Success!"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 400,
                    message = "Delete Fail!"
                });
            }
        }
        
        [HttpGet("items/{orderId}")]
        public async Task<ActionResult> GetListItemByOrderId(int orderId)
        {
            try
            {
                List<OrderDetail> order = await _orderDetailService.GetListOrderDetailsByOrderId(orderId);
                if (order == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "OrderId doesn't exist!"
                    });
                }
                return Ok(new
                {
                    code = 200,
                    message = "Delete Success!",
                    data = order
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("user/{userId}")]
        public async Task<ActionResult> GetListOrderForUser(int userId)
        {
            try
            {
                User user = await _userService.GetByIdAsync(userId);
                if (user == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "User doesn't exist"
                    });
                }
                if (user.UserRole != UserRole.Customer)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "User isn't shipper"
                    });
                }
                var data = await _orderService.GetListOrderForUser(userId);
                return Ok(new
                {
                    code = 200,
                    message = "Get Success!",
                    data = data
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("shipper/{shipperId}")]
        public async Task<ActionResult> GetListOrderForShipper(int shipperId)
        {
            try
            {
                User user = await _userService.GetByIdAsync(shipperId);
                if (user == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Shipper doesn't exist"
                    });
                }
                if(user.UserRole != UserRole.Shipper)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "User isn't shipper"
                    });
                }
                var data = await _orderService.GetListOrderForShipper(shipperId);
                return Ok(new
                {
                    code = 200,
                    message = "Get Success!",
                    data = data
                });
            }
            catch(Exception ex)
            {
                return Ok(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPut("shipper/receive/{orderId}/{shipperId}")]
        public async Task<ActionResult> ShipperReceiveOrder(int orderId, int shipperId)
        {
            try
            {
                User user = await _userService.GetByIdAsync(shipperId);
                if (user == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Shipper doesn't exist"
                    });
                }
                if (user.UserRole != UserRole.Shipper)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "User isn't shipper"
                    });
                }
                Order order = await _orderService.GetByIdAsync(orderId);
                if (order == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Order doesn't exist"
                    });
                }
                order.ShipperId = shipperId;
                order.Status = OrderStatus.Delivering;
                OrderResponseModel rs = await _orderService.UpdateAsync(order);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = rs.Message
                    });
                }
                return Ok(new
                {
                    code = 200,
                    message = "Receive Success!"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        
        [HttpPut("shipper/complete/{orderId}")]
        public async Task<ActionResult> ShipperCompleteOrder(int orderId)
        {
            try
            {
                Order order = await _orderService.GetByIdAsync(orderId);
                if (order == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Order doesn't exist"
                    });
                }
                order.Status = OrderStatus.Delivered;
                order.ReceivedDate = new DateTime();
                OrderResponseModel rs = await _orderService.UpdateAsync(order);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = rs.Message
                    });
                }
                return Ok(new
                {
                    code = 200,
                    message = "Complete Success!"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        
        [HttpPut("shipper/reject/{orderId}/{note}")]
        public async Task<ActionResult> ShipperRejectOrder(int orderId, string note)
        {
            try
            {
                Order order = await _orderService.GetByIdAsync(orderId);
                if (order == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Order doesn't exist"
                    });
                }
                order.Status = OrderStatus.Reject;
                order.Note = note;
                order.ReceivedDate = new DateTime();
                OrderResponseModel rs = await _orderService.UpdateAsync(order);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = rs.Message
                    });
                }
                return Ok(new
                {
                    code = 200,
                    message = "Reject Success!"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpPut("user/reject/{orderId}/{note}")]
        public async Task<ActionResult> UserRejectOrder(int orderId, string note)
        {
            try
            {
                Order order = await _orderService.GetByIdAsync(orderId);
                if (order == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Order doesn't exist"
                    });
                }
                order.Status = OrderStatus.Reject;
                order.Note = note;
                order.ReceivedDate = new DateTime();
                OrderResponseModel rs = await _orderService.UpdateAsync(order);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = rs.Message
                    });
                }
                return Ok(new
                {
                    code = 200,
                    message = "Reject Success!"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpPut("accept-payment/{orderId}")]
        public async Task<ActionResult> AcceptPayment(int orderId)
        {
            try
            {
                Order order = await _orderService.GetByIdAsync(orderId);
                if (order == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Order doesn't exist"
                    });
                }
                var code = 201;
                if(order.Status == OrderStatus.NotPay)
                {
                    code = 200;
                    order.Status = OrderStatus.Processing;
                }
                
                OrderResponseModel rs = await _orderService.UpdateAsync(order);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = rs.Message
                    });
                }
                return Ok(new
                {
                    code = code,
                    message = "Receive Success!"
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
    }
}
