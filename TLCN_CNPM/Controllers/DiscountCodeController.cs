﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;
using TLCN_CNPM.Service;
using TLCN_CNPM.Utils;

namespace TLCN_CNPM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiscountCodeController : BaseController<IDiscountCodeService>
    {
        public DiscountCodeController(IDiscountCodeService service) : base(service)
        {

        }
        [HttpGet]
        public async Task<ActionResult> GetDiscounts([FromQuery] PaginationFilter filter)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var result = await _service.GetAsync(validFilter.PageNumber, validFilter.PageSize);
                var totalElements = await _service.CountAsync();
                return Ok(new
                {
                    data = result,
                    pageNumber = validFilter.PageNumber,
                    pageSize = validFilter.PageSize,
                    totalElements = totalElements,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("user/{id}")]
        public async Task<ActionResult> GetDiscountCodeByUserId(int id)
        {
            try
            {
                var result = await _service.GetDiscountCodeByUserId(id);
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<DiscountCode>> GetDiscountCode(int id)
        {
            var code = await _service.GetByIdAsync(id);

            if (code == null)
            {
                return NotFound();
            }

            return Ok(code);
        }
        [HttpGet("check/{code}")]
        public async Task<ActionResult> GetDiscountByCode(string code)
        {
            try
            {
                var result = await _service.GetDiscountCodeByCode(code);
                if(result == null)
                {
                    return Ok(new
                    {
                        message = "Code doesn't exist",
                        code = 400
                    });
                }
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpPut("use/{code}")]
        public async Task<ActionResult> UseDiscountCode(string code)
        {
            try
            {
                var result = await _service.GetDiscountCodeByCode(code);
                if (result == null)
                {
                    return Ok(new
                    {
                        message = "Code doesn't exist",
                        code = 400
                    });
                }
                result.IsUsed = true;
                var rs = await _service.UpdateAsync(result);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        message = "Update fail",
                        code = 400
                    });
                }
                return Ok(new
                {
                    data = result,
                    code = 200
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddDiscountCode([FromBody] AddDiscountCodeViewModel discountCodeModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are not valid");
            }
            for(int i = 0; i < discountCodeModel.Quantity; i++)
            {
                DiscountCode code = new DiscountCode
                {
                    Desciption = discountCodeModel.Desciption,
                    CreatedDate = new DateTime(),
                    ExpirationDate = discountCodeModel.ExpirationDate,
                    PaymentType = (PaymentType)discountCodeModel.PaymentType,
                    DiscountType = (DiscountType)discountCodeModel.DiscountType,
                    DiscountValue = discountCodeModel.DiscountValue,
                    MinimumBill = discountCodeModel.MinimumBill,
                    Code = StringUtil.RandomString(12, false)
                };
                await _service.AddDiscountCode(code);
            }
            return Ok(new
            {
                code = 200,
                message = "Add Success!"
            });
        }
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateDiscountCodeAsync(int id, [FromBody] UpdateDiscountCodeViewModel viewModel)
        {
            try
            {
                DiscountCode code = await _service.GetByIdAsync(id);
                if (code == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "user doesn't exist"
                    });
                }
                code.Code = viewModel.Code;
                code.Desciption = viewModel.Desciption;
                code.CreatedDate = viewModel.CreatedDate;
                code.ExpirationDate = viewModel.ExpirationDate;
                code.PaymentType = (PaymentType)viewModel.PaymentType;
                code.DiscountType = (DiscountType)viewModel.DiscountType;
                code.DiscountValue = viewModel.DiscountValue;
                code.MinimumBill = viewModel.MinimumBill;
                code.IsUsed = viewModel.IsUsed;
                code.UserId = viewModel.UserId;
             
                var rs = await _service.UpdateAsync(code);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDiscountCode(int id)
        {
            try
            {
                var code = await _service.GetByIdAsync(id);
                if (code == null)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Discount code doesn't exist"
                    });
                }
                var rs = await _service.DeleteAsync(code);
                if (!rs.IsSuccess)
                {
                    return Ok(new
                    {
                        code = 400,
                        message = "Update fail"
                    });
                }

                return Ok(new
                {
                    code = 200,
                    message = "Update success"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    code = 400,
                    message = ex.Message
                });
            }
        }
    }
}
