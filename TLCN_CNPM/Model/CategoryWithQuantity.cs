﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;

namespace TLCN_CNPM.Model
{
    public class CategoryWithQuantity
    {
        //public int Id { get; set; }
        //public string Name { get; set; }
        //public string Icon { get; set; }
        public Category Category { get; set; }
        public int Quantity { get; set; }
    }
}
