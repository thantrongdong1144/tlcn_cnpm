﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Model
{
    public class AddCategoryModelView
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Icon { get; set; }
    }
    public class UpdateCategoryModelView
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Icon { get; set; }
    }
}
