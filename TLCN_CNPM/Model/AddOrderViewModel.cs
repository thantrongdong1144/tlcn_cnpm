﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;

namespace TLCN_CNPM.Model
{
    public class AddOrderViewModel
    {
        [Required]
        public string ReceiverName { get; set; }
        [Required]
        public string ReceiverPhone { get; set; }
        [Required]
        public string ReceiverAddress { get; set; }
        [Required]
        public PaymentType PaymentType { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        public string Note { get; set; }
        [Required]
        public bool IsApplyDiscount { get; set; }
        [Required]
        public int RealTotalCost { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public ICollection<AddOrderDetailViewModel> OrderDetails { get; set; }
    }
    public class AddOrderByAdminViewModel
    {
        [Required]
        public string ReceiverName { get; set; }
        [Required]
        public string ReceiverPhone { get; set; }
        [Required]
        public string ReceiverAddress { get; set; }
        [Required]
        public PaymentType PaymentType { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        public string Note { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public ICollection<AddOrderDetailViewModel> OrderDetails { get; set; }
    }
    public class UpdateOrderViewModel
    {
        [Required]
        public string ReceiverName { get; set; }
        [Required]
        public string ReceiverPhone { get; set; }
        [Required]
        public string ReceiverAddress { get; set; }
        public string Note { get; set; }
        [Required]
        public int RealTotalCost { get; set; }
        [Required]
        public OrderStatus Status { get; set; }
        [Required]
        public ICollection<AddOrderDetailViewModel> OrderDetails { get; set; }
    }
}
