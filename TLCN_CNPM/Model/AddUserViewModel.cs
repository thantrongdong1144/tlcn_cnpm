﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Model
{
    public class AddUserViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Phone { get; set; }
        public string Sex { get; set; }
        public string Email { get; set; }
        public string PassWord { get; set; }
    }

    public class AddUserHasUserrleModelView
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string Avatar { get; set; }

        [Required]
        public string Sex { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public Entity.UserRole UserRole { get; set; }

        [Required]
        public bool Status { get; set; }

        [Required]
        public string PassWord { get; set; }

        [Required]
        public DateTime RegisterDate { get; set; }
        public string ChargeArea { get; set; }

    }
    public class UpdateUserModelView
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string Avatar { get; set; }

        [Required]
        public string Sex { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public Entity.UserRole UserRole { get; set; }

        [Required]
        public bool Status { get; set; }

        [Required]
        public string PassWord { get; set; }
  

    }
}
