﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Model
{
    public class AddMenuDetailViewModel
    {
        public int ProductId { get; set; }
        public int TotalKalo { get; set; }
    }
    public class AddMenuDetailToMenuViewModel
    {
        public int MenuId { get; set; }
        public int ProductId { get; set; }
        public int TotalKalo { get; set; }
    }
    public class UpdateMenuDetailInMenuViewModel
    {
        public int MenuId { get; set; }
        public int ProductId { get; set; }
        public int TotalKalo { get; set; }
    }
}
