﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;

namespace TLCN_CNPM.Model
{
    public class AddOrderDetailViewModel
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int RealPrice { get; set; }
    }
    public class AddOrderDetailToOrderViewModel
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int RealPrice { get; set; }
    }
    public class UpdateOrderDetailInOrderViewModel
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int RealPrice { get; set; }
    }
}
