﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TLCN_CNPM.Model
{
    public class UpdateDiscountProductViewModel
    {
        [Required]
        public List<UpdateDiscountItem> ListDiscount { get; set; }
    }
    public class UpdateDiscountItem
    {
        public int Id { get; set; }
        public int NewPrice { get; set; }
        public int DiscountPrice { get; set; }
        public DateTime ExpirationDiscountDate { get; set; }
    }
}
