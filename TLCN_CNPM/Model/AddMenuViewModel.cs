﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;


namespace TLCN_CNPM.Model
{
    public class AddMenuViewModel
    {
        [Required]
        public string MenuName { get; set; }
        [Required]
        public ICollection<AddMenuDetailViewModel> MenuDetails { get; set; }
    }
    public class AddMenuByAdminViewModel
    {
        [Required]
        public string MenuName { get; set; }
        [Required]
        public ICollection<AddMenuDetailViewModel> MenuDetails { get; set; }
    }
    public class UpdateMenuViewModel
    {
        [Required]
        public string MenuName { get; set; }
        public int RealTotalKalo { get; set; }
        [Required]
        public ICollection<AddMenuDetailViewModel> MenuDetails { get; set; }
    }
}
