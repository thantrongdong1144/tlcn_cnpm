﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Model
{
    public class AddDiscountCodeViewModel
    {
        [Required]
        public string Desciption { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }
        [Required]
        public int PaymentType { get; set; }
        [Required]
        public int DiscountType { get; set; }
        [Required]
        public int DiscountValue { get; set; }
        [Required]
        public int MinimumBill { get; set; }
        [Required]
        public int Quantity { get; set; }
    }

    public class UpdateDiscountCodeViewModel
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Desciption { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }

        [Required]
        public int PaymentType { get; set; }
        [Required]
        public int DiscountType { get; set; }
        [Required]
        public int DiscountValue { get; set; }
        [Required]
        public int MinimumBill { get; set; }
        [Required]
        public bool IsUsed { get; set; }
        [Required]
        public int UserId { get; set; }
    }
}
