﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;

namespace TLCN_CNPM.Model
{
    public class AddProductViewModel
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Picture { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int Price { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public int Like { get; set; }
        [Required]
        public float Star { get; set; }
        [Required]
        public int CategoryId { get; set; }

    }

    public class UpdateProductViewModel
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Picture { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int Price { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public int Like { get; set; }
        [Required]
        public float Star { get; set; }
        [Required]
        public int CategoryId { get; set; }

    }
}
