﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TLCN_CNPM.Model
{
    public class LoginViewModel
    {
            [Required]
            public string UserName { get; set; }

            [Required]
            public string PassWord { get; set; }
    }
    public class ForgotPasswordViewModel
    {
        [Required]
        public string Email { get; set; }
    }
    public class ConfirmPasswordViewModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}
