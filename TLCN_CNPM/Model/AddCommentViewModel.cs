﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Model
{
    public class AddCommentViewModel
    {
        [Required]
        public int Star { get; set; }
        [Required]
        public DateTime CommentDate { get; set; }
        [Required]
        public string CommentContent { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int ProductId { get; set; }
    }

    public class UpdateCommentModelView
    {
        [Required]
        public int Star { get; set; }
        //[Required]
        //public DateTime CommentDate { get; set; }
        [Required]
        public string CommentContent { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int ProductId { get; set; }
    }
}
