﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;

namespace TLCN_CNPM.Model
{
    public class FavoriteProductResponseModel
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
        public IEnumerable<string> Errors { get; set; }
        public FavoriteProduct Product { get; set; }
    }
}
