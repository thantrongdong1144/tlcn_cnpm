﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Repositories
{
    public interface IDiscountCodeRepository
    {
        Task<List<DiscountCode>> GetDiscountCodeByUserId(int id);
        Task<DiscountCode> GetDiscountCodeByCode(string code);
        Task<DiscountCodeResponseModel> UpdateAsync(DiscountCode code);
        Task<DiscountCodeResponseModel> AddDiscountCode(DiscountCode code);
        Task<DiscountCode> GetByIdAsync(int id);
        Task<List<DiscountCode>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        Task<List<DiscountCode>> GetAllAsync();
        Task<DiscountCodeResponseModel> DeleteAsync(DiscountCode code);
    }
    public class DiscountCodeRepository : IDiscountCodeRepository
    {
        MarketDBContext _context;
        public DiscountCodeRepository(MarketDBContext context)
        {
            _context = context;
        }
       
        public async Task<List<DiscountCode>> GetDiscountCodeByUserId(int id)
        {
            return await _context.DiscountCodes.Where(c => c.UserId == id).ToListAsync();
        }
        public async Task<DiscountCode> GetDiscountCodeByCode(string code)
        {
            return await _context.DiscountCodes.FirstOrDefaultAsync(c => c.Code == code);
        }
        public async Task<DiscountCodeResponseModel> UpdateAsync(DiscountCode code)
        {
            _context.Entry(code).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return new DiscountCodeResponseModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    DiscountCode = code
                };
            }
            catch (Exception ex)
            {
                return new DiscountCodeResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<DiscountCodeResponseModel> AddDiscountCode(DiscountCode code)
        {
            try
            {
                await _context.DiscountCodes.AddAsync(code);
                await _context.SaveChangesAsync();
                return (new DiscountCodeResponseModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    DiscountCode = code
                });
            }
            catch (Exception ex)
            {
                return (new DiscountCodeResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }
        }

        public async Task<DiscountCode> GetByIdAsync(int id)
        {
            return await _context.DiscountCodes.AsNoTracking().FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<List<DiscountCode>> GetAsync(int pageNum, int pageSize)
        {
            return await _context.DiscountCodes
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();
        }

        public async Task<int> CountAsync()
        {
            return await _context.DiscountCodes.CountAsync();
        }

        public async Task<List<DiscountCode>> GetAllAsync()
        {
            return await _context.DiscountCodes.AsNoTracking().ToListAsync();
        }

        public async Task<DiscountCodeResponseModel> DeleteAsync(DiscountCode code)
        {
            try
            {
                _context.DiscountCodes.Remove(code);
                await _context.SaveChangesAsync();
                return new DiscountCodeResponseModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            }
            catch (Exception ex)
            {
                return new DiscountCodeResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
