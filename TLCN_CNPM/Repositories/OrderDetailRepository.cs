﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Model;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using Microsoft.EntityFrameworkCore;

namespace TLCN_CNPM.Repositories
{
    public interface IOrderDetailRepository
    {
        Task<OrderDetailResponseModel> AddOrderDetail(OrderDetail orderDetail);
        Task<OrderDetailResponseModel> UpdateAsync(OrderDetail orderDetail);
        Task<OrderDetailResponseModel> DeleteAsync(OrderDetail orderDetail);
        Task<OrderDetail> GetByOrderIdAndProductId(int orderId, int productId);
        Task<List<OrderDetail>> GetListOrderDetailsByOrderId(int id);
    }
    public class OrderDetailRepository : IOrderDetailRepository
    {
        MarketDBContext _context;
        public OrderDetailRepository(MarketDBContext context)
        {
            _context = context;
        }
        public async Task<OrderDetailResponseModel> AddOrderDetail(OrderDetail orderDetail)
        {
            try
            {
                await _context.OrderDetails.AddAsync(orderDetail);
                _context.SaveChanges();
                return (new OrderDetailResponseModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    OrderDetail = orderDetail
                });
            }
            catch (Exception ex)
            {
                return (new OrderDetailResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }
        }
        public async Task<OrderDetailResponseModel> UpdateAsync(OrderDetail orderDetail)
        {
            _context.Entry(orderDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return new OrderDetailResponseModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    OrderDetail = orderDetail
                };
            }
            catch (Exception ex)
            {
                return new OrderDetailResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<OrderDetailResponseModel> DeleteAsync(OrderDetail orderDetail)
        {
            try
            {
                _context.OrderDetails.Remove(orderDetail);
                await _context.SaveChangesAsync();
                return new OrderDetailResponseModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            }
            catch (Exception ex)
            {
                return new OrderDetailResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<OrderDetail> GetByOrderIdAndProductId(int orderId, int productId)
        {
            return await _context.OrderDetails.FirstOrDefaultAsync(details => details.OrderId == orderId && details.ProductId == productId);
        }
        public async Task<List<OrderDetail>> GetListOrderDetailsByOrderId(int id)
        {
            return await _context.OrderDetails.Where(o => o.OrderId == id).ToListAsync();
        }
    }
}
