﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Data;
using TLCN_CNPM.Model;
using Microsoft.EntityFrameworkCore;


namespace TLCN_CNPM.Repositories
{
    public interface ICommentRepository
    {
        Task<CommentResponseModel> AddComment(Comment comment);
        Task<List<Comment>> GetCommentByProductId(int id, int pageNum, int pageSize);
        Task<int> CoutByProductIdAsync(int id);
        Task<CommentResponseModel> UpdateComment(Comment cmt);
        Task<Comment> GetByIdAsync(int id);
        Task<List<Comment>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        Task<List<Comment>> GetAllAsync();
        Task<CommentResponseModel> DeleteAsync(Comment cmt);
    }
    public class CommentRepository: ICommentRepository
    {
        MarketDBContext _context;
        public CommentRepository(MarketDBContext context)
        {
            _context = context;
        }
        public async Task<CommentResponseModel> AddComment(Comment comment)
        {
            try
            {
                await _context.Comments.AddAsync(comment);
                await _context.SaveChangesAsync();
                return (new CommentResponseModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    Comment = comment
                });
            }
            catch (Exception ex)
            {
                return (new CommentResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }
        }
        public async Task<List<Comment>> GetCommentByProductId(int id, int pageNum, int pageSize)
        {
            return await _context.Comments.Where(c => c.ProductId == id && c.User.RecordStatus != EntityStatus.Deleted).Skip((pageNum - 1) * pageSize)
            .Take(pageSize).ToListAsync();
        }
        public async Task<int> CoutByProductIdAsync(int id)
        {
            return await _context.Comments.Where(c => c.ProductId == id && c.User.RecordStatus != EntityStatus.Deleted).CountAsync();
        }


        public async Task<CommentResponseModel> UpdateComment(Comment cmt)
        {
            _context.Entry(cmt).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return new CommentResponseModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    Comment = cmt
                };
            }
            catch (Exception ex)
            {
                return new CommentResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        

        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _context.Comments.AsNoTracking().FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<List<Comment>> GetAsync(int pageNum, int pageSize)
        {
            return await _context.Comments
            .Where(p => p.User.RecordStatus != EntityStatus.Deleted)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();
        }

        public async Task<int> CountAsync()
        {
            return await _context.Comments.Where(p => p.User.RecordStatus != EntityStatus.Deleted).CountAsync();
        }

        public async Task<List<Comment>> GetAllAsync()
        {
            return await _context.Comments.AsNoTracking().Where(p => p.User.RecordStatus != EntityStatus.Deleted).ToListAsync();
        }

        public async Task<CommentResponseModel> DeleteAsync(Comment cmt)
        {
            try
            {
                _context.Comments.Remove(cmt);
                await _context.SaveChangesAsync();
                return new CommentResponseModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            }
            catch (Exception ex)
            {
                return new CommentResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
