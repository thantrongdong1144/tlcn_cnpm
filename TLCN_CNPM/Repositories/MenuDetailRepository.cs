﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Model;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using Microsoft.EntityFrameworkCore;


namespace TLCN_CNPM.Repositories
{
    public interface IMenuDetailRepository
    {
        Task<MenuDetailRepositoryModel> AddMenuDetail(MenuDetail menuDetail);
        Task<MenuDetailRepositoryModel> UpdateAsync(MenuDetail menuDetail);
        Task<MenuDetailRepositoryModel> DeleteAsync(MenuDetail menuDetail);
        Task<MenuDetail> GetByMenuIdAndProductId(int menuId, int productId);
        Task<List<MenuDetail>> GetListMenuDetailsByMenuId(int id);
    }

    public class MenuDetailRepository: IMenuDetailRepository
    {
        MarketDBContext _context;
        public MenuDetailRepository(MarketDBContext context)
        {
            _context = context;
        }
        public async Task<MenuDetailRepositoryModel> AddMenuDetail(MenuDetail menuDetail)
        {
            try
            {
                await _context.MenuDetails.AddAsync(menuDetail);
                _context.SaveChanges();
                return (new MenuDetailRepositoryModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    MenuDetail = menuDetail
                });
            }
            catch (Exception ex)
            {
                return (new MenuDetailRepositoryModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }
        }
        public async Task<MenuDetailRepositoryModel> UpdateAsync(MenuDetail menuDetail)
        {
            _context.Entry(menuDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return new MenuDetailRepositoryModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    MenuDetail = menuDetail
                };
            }
            catch (Exception ex)
            {
                return new MenuDetailRepositoryModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<MenuDetailRepositoryModel> DeleteAsync(MenuDetail menuDetail)
        {
            try
            {
                _context.MenuDetails.Remove(menuDetail);
                await _context.SaveChangesAsync();
                return new MenuDetailRepositoryModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            }
            catch (Exception ex)
            {
                return new MenuDetailRepositoryModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<MenuDetail> GetByMenuIdAndProductId(int menuId, int productId)
        {
            return await _context.MenuDetails.FirstOrDefaultAsync(details => details.MenuId == menuId && details.ProductId == productId);
        }
        public async Task<List<MenuDetail>> GetListMenuDetailsByMenuId(int id)
        {
            return await _context.MenuDetails.Where(o => o.MenuId == id && o.Product.RecordStatus != EntityStatus.Deleted && o.Product.Category.RecordStatus != EntityStatus.Deleted).ToListAsync();
        }
    }
}
