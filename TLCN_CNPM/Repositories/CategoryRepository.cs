﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Data;
using Microsoft.EntityFrameworkCore;
using TLCN_CNPM.Model;
using TLCN_CNPM.Utils;

namespace TLCN_CNPM.Repositories
{
    public interface ICategoryRepository
    {
        Task<List<Category>> GetAllAsync();
        Task<Category> GetByIdAsync(int id);
        Task<List<Category>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        Task<List<Category>> GetAsyncByAdmin(int pageNum, int pageSize, string search);
        Task<int> CountAsyncByAdmin(string search);
        Task<CategoryResponseModel> AddAsync(Category cate);
        Task<CategoryResponseModel> UpdateAsync(Category cate);
        Task<CategoryResponseModel> DeleteAsync(Category cate);
        Task<List<CategoryWithQuantity>> GetCategoryWithTotalProduct();
    }
    public class CategoryRepository : ICategoryRepository
    {
        MarketDBContext _context;
        public CategoryRepository(MarketDBContext context)
        {
            _context = context;
        }
        public async Task<Category> GetByIdAsync(int id)
        {
            return await _context.Categories.FirstOrDefaultAsync(u => u.Id == id);
        }
        public async Task<List<Category>> GetAllAsync()
        {
            return await _context.Categories.Where(p => p.RecordStatus != EntityStatus.Deleted).AsNoTracking().ToListAsync();
        }
        public async Task<List<Category>> GetAsync(int pageNum, int pageSize)
        {
            return await _context.Categories
            .Where(p => p.RecordStatus != EntityStatus.Deleted)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();
        }
        public async Task<int> CountAsync()
        {
            return await _context.Categories.Where(p => p.RecordStatus != EntityStatus.Deleted).CountAsync();
        }
        public async Task<List<Category>> GetAsyncByAdmin(int pageNum, int pageSize, string search)
        {
            string searchStr = "";
            if (search != null)
            {
                searchStr = StringUtil.RemoveSign4VietnameseString(search.ToLower());
            }
            var product = await _context.Categories.Select(p => new { str = StringUtil.RemoveSign4VietnameseString(p.Name.ToLower()), cate = p }).ToListAsync();
            return product.Where(p => p.str.Contains(searchStr))
            .Select(p => p.cate)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToList();
        }
        public async Task<int> CountAsyncByAdmin(string search)
        {
            string searchStr = "";
            if (search != null)
            {
                searchStr = StringUtil.RemoveSign4VietnameseString(search.ToLower());
            }
            var product = await _context.Categories.Select(p => new { str = StringUtil.RemoveSign4VietnameseString(p.Name.ToLower()), cate = p }).ToListAsync();
            return product.Where(p => p.str.Contains(searchStr)).Count();
        }
        public async Task<CategoryResponseModel> AddAsync(Category cate)
        {
            try
            {
                await _context.Categories.AddAsync(cate);
                await _context.SaveChangesAsync();
                return (new CategoryResponseModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    Category = cate
                });
            }
            catch (Exception ex)
            {
                return (new CategoryResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }
        }
        public async Task<CategoryResponseModel> UpdateAsync(Category cate)
        {
            _context.Entry(cate).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return new CategoryResponseModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    Category = cate
                };
            }
            catch (Exception ex)
            {
                return new CategoryResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<CategoryResponseModel> DeleteAsync(Category cate)
        {
            try
            {
                //_context.Categories.Remove(cate);
                cate.RecordStatus = EntityStatus.Deleted;
                //_context.Entry(cate).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return new CategoryResponseModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            } catch( Exception ex)
            {
                return new CategoryResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<List<CategoryWithQuantity>> GetCategoryWithTotalProduct()
        {
            //return await (from cate in _context.Categories
            //              join pro in _context.Products on cate.Id equals pro.CategoryId
            //              group pro by pro.Category into proGroup
            //              select new CategoryWithQuantity{ Category = proGroup.Key, Qunatity = proGroup.Count() }).ToListAsync();
            return await (from cate in _context.Categories
                          where cate.RecordStatus != EntityStatus.Deleted
                          select new CategoryWithQuantity { Category = cate, Quantity = _context.Products.Where(p => p.RecordStatus != EntityStatus.Deleted).Count(p => p.CategoryId == cate.Id) }).ToListAsync();
        }
    }
}
