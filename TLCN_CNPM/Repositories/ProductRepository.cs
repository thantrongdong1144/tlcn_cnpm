﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Data;
using Microsoft.EntityFrameworkCore;
using TLCN_CNPM.Model;
using TLCN_CNPM.Utils;

namespace TLCN_CNPM.Repositories
{
    public interface IProductRepository
    {
        Task<List<Product>> GetAllAsync();
        Task<Product> GetByIdAsync(int id);
        Task<List<Product>> GetByCategoryIdAsync(int id, int pageNum, int pageSize);
        Task<List<Product>> GetAsync(int pageNum, int pageSize);
        Task<List<Product>> GetByProductKeyAsync(string key, int pageNum, int pageSize);
        Task<List<Product>> GetProductsByAdmin(int pageNum, int pageSize, string search);
        Task<List<Product>> GetProductsByDiscount(int pageNum, int pageSize, int isDiscount);
        Task<int> CountAsyncByDiscount(int isDiscount);
        Task<int> CountAsync();
        Task<int> CountAsyncByAdmin(string search);
        Task<int> CountAsyncByCategoryId(int id);
        Task<int> CountAsyncByProductKey(string key);
        Task<List<Product>> GetPopularProduct();
        Task<List<Product>> GetNewProduct();
        Task<List<Product>> GetPopularProductLastWeek();
        Task<ProductResponseModel> AddAsync(Product prod);
        Task<ProductResponseModel> UpdateAsync(Product prod);
        Task<ProductResponseModel> DeleteAsync(Product prod);
    }
    public class ProductRepository: IProductRepository
    {
        MarketDBContext _context;
        public ProductRepository(MarketDBContext context)
        {
            _context = context;
        }
        public async Task<Product> GetByIdAsync(int id)
        {
            return await _context.Products.Include(p => p.Category).FirstOrDefaultAsync(u => u.Id == id);
        }
        public async Task<List<Product>> GetByCategoryIdAsync(int id, int pageNum, int pageSize)
        {
            return await _context.Products.Where(p => p.CategoryId == id && p.RecordStatus != EntityStatus.Deleted).Skip((pageNum - 1) * pageSize)
            .Take(pageSize).ToListAsync();
        }
        public async Task<List<Product>> GetByProductKeyAsync(string key, int pageNum, int pageSize)
        {
            return await _context.Products.Where(p => p.Name.Contains(key) && p.RecordStatus != EntityStatus.Deleted && p.Category.RecordStatus != EntityStatus.Deleted).Skip((pageNum - 1) * pageSize)
           .Take(pageSize).ToListAsync();
        }
        public async Task<List<Product>> GetAllAsync()
        {
            return await _context.Products.Include(p => p.Category).Where(p => p.RecordStatus != EntityStatus.Deleted && p.Category.RecordStatus != EntityStatus.Deleted).ToListAsync();
        }
        public async Task<List<Product>> GetAsync(int pageNum, int pageSize)
        {
            return await _context.Products.OrderBy(p => p.CreatedDate)
            .Where(p => p.RecordStatus != EntityStatus.Deleted && p.Category.RecordStatus != EntityStatus.Deleted)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();
        }

        public async Task<List<Product>> GetProductsByAdmin(int pageNum, int pageSize, string search)
        {
            string searchStr = "";
            if(search != null)
            {
                searchStr = StringUtil.RemoveSign4VietnameseString(search.ToLower());
            }
            var product = await _context.Products.OrderBy(p => p.CreatedDate).Select(p => new { str = StringUtil.RemoveSign4VietnameseString(p.Name.ToLower()), product = p }).ToListAsync();
            return product.Where(p => p.str.Contains(searchStr))
            .Select(p => p.product)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToList();
        }
        public async Task<int> CountAsyncByAdmin(string search)
        {
            string searchStr = "";
            if (search != null)
            {
                searchStr = StringUtil.RemoveSign4VietnameseString(search.ToLower());
            }
            var product = await _context.Products.OrderBy(p => p.CreatedDate).Select(p => new { str = StringUtil.RemoveSign4VietnameseString(p.Name.ToLower()), product = p }).ToListAsync();
            return product.Where(p => p.str.Contains(searchStr)).Count();
        }
        public async Task<List<Product>> GetProductsByDiscount(int pageNum, int pageSize, int isDiscount)
        {
            var today = DateTime.Today;
            //var max = new DateTime(today.Year, today.Month, 1); // first of this month
            //var min = max.AddMonths(1); // first of last month
            //var current = (from lm in _context.Orders
            //                 where lm.OrderDate >= min && lm.OrderDate < max && lm.ShipperId == 1
            //                 select lm).ToList();
            if (isDiscount == 1)
            {
                return await _context.Products.Include(p => p.Category).Where(p =>
                p.RecordStatus != EntityStatus.Deleted
                && p.Category.RecordStatus != EntityStatus.Deleted && p.DiscountPrice > 0 && p.ExpirationDiscountDate >= today)
                .OrderBy(el => el.ExpirationDiscountDate)
                .Skip((pageNum - 1) * pageSize)
                .Take(pageSize)
               .ToListAsync();
            } else if (isDiscount == 2)
            {
                return await _context.Products.Include(p => p.Category).Where(p =>
                p.RecordStatus != EntityStatus.Deleted
                && p.Category.RecordStatus != EntityStatus.Deleted && (p.DiscountPrice == 0 || p.ExpirationDiscountDate < today))
                .Skip((pageNum - 1) * pageSize)
                .Take(pageSize)
               .ToListAsync();
            } else
            {
                return await _context.Products.Include(p => p.Category).Where(p =>
                p.RecordStatus != EntityStatus.Deleted
                && p.Category.RecordStatus != EntityStatus.Deleted)
                .Skip((pageNum - 1) * pageSize)
                .Take(pageSize)
               .ToListAsync();
            }
            
        }
        public async Task<int> CountAsyncByDiscount(int isDiscount)
        {
            var today = DateTime.Today;
            if (isDiscount == 1)
            {
                return await _context.Products.Include(p => p.Category).Where(p =>
                p.RecordStatus != EntityStatus.Deleted
                && p.Category.RecordStatus != EntityStatus.Deleted && p.DiscountPrice > 0 && p.ExpirationDiscountDate >= today)
               .CountAsync();
            }
            else if (isDiscount == 2)
            {
                return await _context.Products.Include(p => p.Category).Where(p =>
                p.RecordStatus != EntityStatus.Deleted
                && p.Category.RecordStatus != EntityStatus.Deleted && (p.DiscountPrice == 0 || p.ExpirationDiscountDate < today))
               .CountAsync();
            }
            else
            {
                return await _context.Products.Include(p => p.Category).Where(p =>
                p.RecordStatus != EntityStatus.Deleted
                && p.Category.RecordStatus != EntityStatus.Deleted)
               .CountAsync();
            }
        }
        public async Task<int> CountAsync()
        {
            return await _context.Products.Where(p => p.RecordStatus != EntityStatus.Deleted && p.Category.RecordStatus != EntityStatus.Deleted).CountAsync();
        }
        
        public async Task<int> CountAsyncByCategoryId(int id)
        {
            return await _context.Products.Where(p => p.CategoryId == id && p.RecordStatus != EntityStatus.Deleted && p.Category.RecordStatus != EntityStatus.Deleted).CountAsync();
        }
        public async Task<int> CountAsyncByProductKey(string key)
        {
            return await _context.Products.Where(p => p.Name.Contains(key) && p.RecordStatus != EntityStatus.Deleted && p.Category.RecordStatus != EntityStatus.Deleted).CountAsync();
        }
        public async Task<List<Product>> GetPopularProduct()
        {
            return await _context
                .Products
                .FromSqlRaw("select p.* from Product p, (select TOP 10 a.ProductId, SUM(a.Quantity) as Quantity from Orderdetail a GROUP BY a.ProductId order by  Quantity desc) q where p.Id = q.ProductId and p.RecordStatus <> 1")
                .ToListAsync();
        }
        public async Task<List<Product>> GetNewProduct()
        {
            return await _context
                .Products
                .FromSqlRaw("select TOP 10 * from Product where RecordStatus <> 1 order by Product.CreatedDate asc")
                .ToListAsync();
        }
        public async Task<List<Product>> GetPopularProductLastWeek()
        {
            string sql = "select k.* from Product k, (select TOP 10 p.ProductId, SUM(p.Quantity) as Quantity"
            + " from(select b.ProductId, b.Quantity from Orders a, OrderDetail b where a.Id = b.OrderId and DATEPART(WEEK, a.OrderDate) = DATEPART(WEEK, GETDATE()) - 1) p"
            + " group by p.ProductId"
            + " order by Quantity desc) q where k.Id = q.ProductId and k.RecordStatus <> 1";
            return await _context
                .Products
                .FromSqlRaw(sql)
                .ToListAsync();
        }

        public async Task<ProductResponseModel> AddAsync(Product prod)
        {
            try
            {
                await _context.Products.AddAsync(prod);
                await _context.SaveChangesAsync();
                return (new ProductResponseModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    Product = prod
                });
            }
            catch (Exception ex)
            {
                return (new ProductResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }
        }

        public async Task<ProductResponseModel> UpdateAsync(Product prod)
        {
            try
            {
                _context.Entry(prod).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return new ProductResponseModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    Product = prod
                };
            }
            catch (Exception ex)
            {
                return new ProductResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<ProductResponseModel> DeleteAsync(Product prod)
        {
            try
            {
                prod.RecordStatus = EntityStatus.Deleted;
                //_context.Products.Remove(prod);
                //await _context.SaveChangesAsync();
                _context.Entry(prod).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return new ProductResponseModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            }
            catch (Exception ex)
            {
                return new ProductResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
