﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Data;
using TLCN_CNPM.Model;
using Microsoft.EntityFrameworkCore;

namespace TLCN_CNPM.Repositories
{
    public interface IMenuRepository
    {
        Task<MenuRepositoryModel> AddMenu(Menu menu);
        Task<List<Menu>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        Task<MenuRepositoryModel> DeleteAsync(Menu menu);
        Task<Menu> GetByIdAsync(int id);
        Task<MenuRepositoryModel> UpdateAsync(Menu menu);
        
    }
    public class MenuRepository: IMenuRepository
    {
        MarketDBContext _context;
        public MenuRepository(MarketDBContext context)
        {
            _context = context;
        }
        public async Task<MenuRepositoryModel> AddMenu(Menu menu)
        {
            try
            {
                await _context.Menus.AddAsync(menu);
                await _context.SaveChangesAsync();
                return (new MenuRepositoryModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    Menu = menu
                });
            }
            catch (Exception ex)
            {
                return (new MenuRepositoryModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }

        }
        public async Task<List<Menu>> GetAsync(int pageNum, int pageSize)
        {
            return await _context.Menus
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();
        }
        
        public async Task<int> CountAsync()
        {
            return await _context.Menus.CountAsync();
        }

        public async Task<MenuRepositoryModel> DeleteAsync(Menu menu)
        {
            try
            {
                _context.Menus.Remove(menu);
                await _context.SaveChangesAsync();
                return new MenuRepositoryModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            }
            catch (Exception ex)
            {
                return new MenuRepositoryModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Menu> GetByIdAsync(int id)
        {
            return await _context.Menus.FirstOrDefaultAsync(u => u.Id == id);
        }
        public async Task<MenuRepositoryModel> UpdateAsync(Menu menu)
        {
            _context.Entry(menu).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return new MenuRepositoryModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    Menu = menu
                };
            }
            catch (Exception ex)
            {
                return new MenuRepositoryModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        
    }
}
