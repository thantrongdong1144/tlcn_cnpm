﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Data;
using TLCN_CNPM.Model;
using Microsoft.EntityFrameworkCore;
using TLCN_CNPM.Utils;

namespace TLCN_CNPM.Repositories
{
    public interface IOrderRepository
    {
        Task<OrderResponseModel> AddOrder(Order order);
        Task<List<Order>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        int CountAsyncByStatusAndShipperId(OrderStatus status, int Id);
        Task<List<Order>> GetAsyncByAdmin(int pageNum, int pageSize, string search);
        Task<List<Order>> GetAsyncByStatus(int pageNum, int pageSize, OrderStatus status);
        Task<int> CountAsyncByAdmin(string search);
        Task<int> CountAsyncByStatus(OrderStatus status);
        Task<OrderResponseModel> DeleteAsync(Order order);
        Task<Order> GetByIdAsync(int id);
        Task<OrderResponseModel> UpdateAsync(Order order);
        Task<List<Order>> GetListOrderForShipper(int shipperId);
        Task<List<Order>> GetListOrderForUser(int userId);
    }
    public class OrderRepository: IOrderRepository
    {
        MarketDBContext _context;
        public OrderRepository(MarketDBContext context)
        {
            _context = context;
        }
        public async Task<OrderResponseModel> AddOrder(Order order)
        {
            try
            {
                await _context.Orders.AddAsync(order);
                await _context.SaveChangesAsync();
                return (new OrderResponseModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    Order = order
                });
            }
            catch (Exception ex)
            {
                return (new OrderResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }

        }
        public async Task<List<Order>> GetAsync(int pageNum, int pageSize)
        {
            return await _context.Orders.OrderByDescending(or => or.OrderDate)
            .Where(p => p.RecordStatus != EntityStatus.Deleted && p.User.RecordStatus != EntityStatus.Deleted)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();
        }
        public async Task<List<Order>> GetListOrderForShipper(int shipperId)
        {
            return await _context.Orders.Where(or => (or.RecordStatus != EntityStatus.Deleted && or.User.RecordStatus != EntityStatus.Deleted && or.Status == OrderStatus.Approved) || or.ShipperId == shipperId).ToListAsync();
        }
        public async Task<int> CountAsync()
        {
            return await _context.Orders.Where(p => p.RecordStatus != EntityStatus.Deleted && p.User.RecordStatus != EntityStatus.Deleted).CountAsync();
        }
        public async Task<List<Order>> GetAsyncByAdmin(int pageNum, int pageSize, string search)
        {
            string searchStr = "";
            if (search != null)
            {
                searchStr = StringUtil.RemoveSign4VietnameseString(search.ToLower());
            }
            var product = await _context.Orders.OrderByDescending(or => or.OrderDate).Select(p => new { str = StringUtil.RemoveSign4VietnameseString(p.Id.ToString().ToLower()), cate = p }).ToListAsync();
            return product.Where(p => p.str.Contains(searchStr))
            .Select(p => p.cate)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToList();
        }

        public async Task<List<Order>> GetAsyncByStatus(int pageNum, int pageSize, OrderStatus status)
        {
            return await _context.Orders.Where(or => (or.RecordStatus != EntityStatus.Deleted && or.User.RecordStatus != EntityStatus.Deleted && or.Status == status)).OrderBy(o => o.OrderDate).ToListAsync();
        }
        public async Task<int> CountAsyncByStatus(OrderStatus status)
        {
            return await _context.Orders.Where(or => (or.RecordStatus != EntityStatus.Deleted && or.User.RecordStatus != EntityStatus.Deleted && or.Status == status)).CountAsync();
        }
        public int CountAsyncByStatusAndShipperId(OrderStatus status, int Id)
        {
            return _context.Orders.Where(or => (or.Status == status && or.ShipperId == Id)).Count();
        }


        public async Task<int> CountAsyncByAdmin(string search)
        {
            string searchStr = "";
            if (search != null)
            {
                searchStr = StringUtil.RemoveSign4VietnameseString(search.ToLower());
            }
            var product = await _context.Orders.OrderByDescending(or => or.OrderDate).Select(p => new { str = StringUtil.RemoveSign4VietnameseString(p.Id.ToString().ToLower()), cate = p }).ToListAsync();
            return product.Where(p => p.str.Contains(searchStr))
            .Select(p => p.cate).Count();
        }

        public async Task<OrderResponseModel> DeleteAsync(Order order)
        {
            try
            {
                //_context.Orders.Remove(order);
                order.RecordStatus = EntityStatus.Deleted;
                _context.Entry(order).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return new OrderResponseModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            }
            catch (Exception ex)
            {
                return new OrderResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Order> GetByIdAsync(int id)
        {
            return await _context.Orders.FirstOrDefaultAsync(u => u.Id == id);
        }
        public async Task<OrderResponseModel> UpdateAsync(Order order)
        {
            _context.Entry(order).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return new OrderResponseModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    Order = order
                };
            }
            catch (Exception ex)
            {
                return new OrderResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<List<Order>> GetListOrderForUser(int userId)
        {
            return await _context.Orders.Where(p => p.RecordStatus != EntityStatus.Deleted).Where(or => or.Status == OrderStatus.Approved || or.UserId == userId).ToListAsync();
        }
    }
}
