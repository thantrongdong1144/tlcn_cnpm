﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Data;
using TLCN_CNPM.Model;
using TLCN_CNPM.Entity;
using Microsoft.EntityFrameworkCore;
using Dapper;

namespace TLCN_CNPM.Repositories
{
    public interface IDashBoardRepository
    {
        Task<IEnumerable<RevenueModel>> GetRevenueOrderByMonth();
        Task<IEnumerable<RevenueModel>> GetRejectTotalOrderByMonth();
        Task<IEnumerable<RevenueModel>> GetNumOfOrderByMonth();
        Task<IEnumerable<dynamic>> GetProductTotalOrderByCategory();
        Task<IEnumerable<dynamic>> TotalNumberOfSuccessOrder();
        Task<IEnumerable<dynamic>> TotalRevenue();
        Task<IEnumerable<dynamic>> TotalUser();
        Task<IEnumerable<dynamic>> TotalRejectOrder();
    }
    public class DashBoardRepository : IDashBoardRepository
    {
        MarketDBContext _context;
        public DashBoardRepository(MarketDBContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<RevenueModel>> GetRevenueOrderByMonth()
        {
            await using var connection = _context.Database.GetDbConnection();
            await _context.Database.OpenConnectionAsync();

            var results =
                await connection
                    .QueryAsync<RevenueModel>("select DATEPART(MONTH, OrderDate) as Month, sum(RealTotalCost) as Total from Orders where DATEPART(YEAR, OrderDate) = DATEPART(YEAR, GETDATE()) and Status = 3 group by DATEPART(MONTH, OrderDate)");
            await _context.Database.CloseConnectionAsync();

            return results;
        }
        public async Task<IEnumerable<RevenueModel>> GetRejectTotalOrderByMonth()
        {
            await using var connection = _context.Database.GetDbConnection();
            await _context.Database.OpenConnectionAsync();

            var results =
                await connection
                    .QueryAsync<RevenueModel>("select DATEPART(MONTH, OrderDate) as Month, sum(RealTotalCost) as Total from Orders where DATEPART(YEAR, OrderDate) = DATEPART(YEAR, GETDATE()) and Status = 4 group by DATEPART(MONTH, OrderDate)");
            await _context.Database.CloseConnectionAsync();

            return results;
        }
        public async Task<IEnumerable<RevenueModel>> GetNumOfOrderByMonth()
        {
            await using var connection = _context.Database.GetDbConnection();
            await _context.Database.OpenConnectionAsync();

            var results =
                await connection
                    .QueryAsync<RevenueModel>("select DATEPART(MONTH, OrderDate) as Month, count(Id) as Total from Orders where DATEPART(YEAR, OrderDate) = DATEPART(YEAR, GETDATE()) and Status = 3 group by DATEPART(MONTH, OrderDate)");
            await _context.Database.CloseConnectionAsync();

            return results;
        }
        public async Task<IEnumerable<dynamic>> GetProductTotalOrderByCategory()
        {
            await using var connection = _context.Database.GetDbConnection();
            await _context.Database.OpenConnectionAsync();

            var results =
                await connection
                    .QueryAsync<dynamic>("select  c.Id, c.Name, sum(q.Quantity) as Quantity, c.Icon from Product p, (select TOP 10 a.ProductId, SUM(a.Quantity) as Quantity from Orderdetail a GROUP BY a.ProductId order by  Quantity desc) q, Category c where p.Id = q.ProductId and p.CategoryId = c.Id group by c.Id, c.Name, c.Icon");
            await _context.Database.CloseConnectionAsync();

            return results;
        }
        public async Task<IEnumerable<dynamic>> TotalNumberOfSuccessOrder()
        {
            await using var connection = _context.Database.GetDbConnection();
                await _context.Database.OpenConnectionAsync();

            var results =
                await connection
                    .QueryAsync<dynamic>("select count(Id) as Total from Orders where Status = 3");
            await _context.Database.CloseConnectionAsync();

            return results;
        }
        public async Task<IEnumerable<dynamic>> TotalRevenue()
        {
            await using var connection = _context.Database.GetDbConnection();
                await _context.Database.OpenConnectionAsync();

            var results =
                await connection
                    .QueryAsync<dynamic>("select sum(RealTotalCost) as Total from Orders where Status = 3");
            await _context.Database.CloseConnectionAsync();

            return results;
        }
        public async Task<IEnumerable<dynamic>> TotalUser()
        {
            await using var connection = _context.Database.GetDbConnection();
            await _context.Database.OpenConnectionAsync();

            var results =
                await connection
                    .QueryAsync<dynamic>("select count(Id) as total from Users where UserRole = 2");
            await _context.Database.CloseConnectionAsync();

            return results;
        }
        public async Task<IEnumerable<dynamic>> TotalRejectOrder()
        {
            await using var connection = _context.Database.GetDbConnection();
            await _context.Database.OpenConnectionAsync();

            var results =
                await connection
                    .QueryAsync<dynamic>("select count(Id) as total from Orders where Status = 4");
            await _context.Database.CloseConnectionAsync();

            return results;
        }
    }
}
