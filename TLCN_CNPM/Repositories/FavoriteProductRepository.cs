﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Repositories
{
    public interface IFavoriteProductRepository
    {
        Task<List<FavoriteProduct>> GetListFavoriteProductByUserId(int id);
        Task<FavoriteProduct> GetFavoriteProduct(int userId, int productId);
        Task<FavoriteProductResponseModel> AddAsync(FavoriteProduct prod);
        Task<FavoriteProductResponseModel> UpdateAsync(FavoriteProduct prod);
        Task<FavoriteProductResponseModel> DeleteAsync(FavoriteProduct prod);
    }
    public class FavoriteProductRepository: IFavoriteProductRepository
    {
        MarketDBContext _context;
        public FavoriteProductRepository(MarketDBContext context)
        {
            _context = context;
        }
        public async Task<List<FavoriteProduct>> GetListFavoriteProductByUserId(int id)
        {
            return await _context.FavoriteProducts.Where(p => p.UserId == id && p.Product.RecordStatus != EntityStatus.Deleted && p.Product.Category.RecordStatus != EntityStatus.Deleted).ToListAsync();
        }
        public async Task<FavoriteProduct> GetFavoriteProduct(int userId, int productId)
        {
            return await _context.FavoriteProducts.FirstOrDefaultAsync(p => p.UserId == userId && p.ProductId == productId);
        }
        public async Task<FavoriteProductResponseModel> AddAsync(FavoriteProduct prod)
        {
            try
            {
                await _context.FavoriteProducts.AddAsync(prod);
                await _context.SaveChangesAsync();
                return (new FavoriteProductResponseModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    Product = prod
                });
            }
            catch (Exception ex)
            {
                return (new FavoriteProductResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }
        }
        public async Task<FavoriteProductResponseModel> UpdateAsync(FavoriteProduct prod)
        {
            try
            {
                _context.Entry(prod).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return new FavoriteProductResponseModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    Product = prod
                };
            }
            catch (Exception ex)
            {
                return new FavoriteProductResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<FavoriteProductResponseModel> DeleteAsync(FavoriteProduct prod)
        {
            try
            {
                _context.FavoriteProducts.Remove(prod);
                await _context.SaveChangesAsync();
                return new FavoriteProductResponseModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            }
            catch (Exception ex)
            {
                return new FavoriteProductResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
