﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Data;
using Microsoft.EntityFrameworkCore;
using TLCN_CNPM.Model;
using TLCN_CNPM.Utils;

namespace TLCN_CNPM.Repositories
{
    public interface IUserRepository
    {
        Task<List<User>> GetAllAsync();
        Task<User> GetByIdAsync(int id);
        Task<List<User>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        Task<List<User>> GetAsyncByAdmin(int pageNum, int pageSize, string search);
        Task<List<User>> GetShipperAsync(int pageNum, int pageSize);
        Task<int> CountShipperAsync();

        Task<int> CountAsyncByAdmin(string search);
        Task<UserResponseModel> LoginAsync(LoginViewModel loginView);
        Task<UserResponseModel> UpdateUser(User user);
        Task<User> GetByEmailAsync(string email);
        Task<User> GetByUserNameAsync(string userName);
        Task<UserResponseModel> AddUserAsync(User user);
        Task<UserResponseModel> DeleteAsync(User user);
    }
    public class UserRepository : IUserRepository
    {
        MarketDBContext _context;
        public UserRepository(MarketDBContext context)
        {
            _context = context;
        }

        
        public async Task<User> GetByIdAsync(int id)
        {
            return await _context.Users.AsNoTracking().Where(p => p.RecordStatus != EntityStatus.Deleted).FirstOrDefaultAsync(u => u.Id == id);
        }
        public async Task<List<User>> GetAllAsync()
        {
            return await _context.Users.AsNoTracking().Where(p => p.RecordStatus != EntityStatus.Deleted).ToListAsync();
        }
        public async Task<UserResponseModel> LoginAsync(LoginViewModel loginView)
        {
            var user = await GetByUserNameAsync(loginView.UserName);
            if(user == null)
            {
                return new UserResponseModel
                {
                    IsSuccess = false,
                    Message = "There is no user with that email address"
                };
            }
            if(user.PassWord == loginView.PassWord)
            {
                return new UserResponseModel
                {
                    IsSuccess = true,
                    Message = "Login success",
                    User = user
                };
            } else
            {
                return new UserResponseModel
                {
                    IsSuccess = false,
                    Message = "Passwork is invalid"
                };
            }
        }
        public async Task<UserResponseModel> UpdateUser(User user)
        {
            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return new UserResponseModel
                {
                    IsSuccess = true,
                    Message = "Update success",
                    User = user
                };
            }
            catch (Exception ex)
            {
                return new UserResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<UserResponseModel> AddUserAsync(User user)
        {
            try
            {
                await _context.Users.AddAsync(user);
                await _context.SaveChangesAsync();
                return (new UserResponseModel
                {
                    IsSuccess = true,
                    Message = "Add success!",
                    User = user
                });
            }
            catch (Exception ex)
            {
                return (new UserResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                });
            }
        }
        public async Task<User> GetByEmailAsync(string email)
        {
            return await _context.Users.AsNoTracking().Where(p => p.RecordStatus != EntityStatus.Deleted).FirstOrDefaultAsync(u => u.Email == email);
        }
        public async Task<User> GetByUserNameAsync(string userName)
        {
            return await _context.Users.AsNoTracking().Where(p => p.RecordStatus != EntityStatus.Deleted).FirstOrDefaultAsync(u => u.Username == userName);
        }

        public async Task<List<User>> GetAsync(int pageNum, int pageSize)
        {
            return await _context.Users.Where(p => p.RecordStatus != EntityStatus.Deleted)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();
        }
        public async Task<int> CountAsync()
        {
            return await _context.Users.CountAsync();
        }
        public async Task<List<User>> GetShipperAsync(int pageNum, int pageSize)
        {
            return await _context.Users.Where(p => p.RecordStatus != EntityStatus.Deleted && p.UserRole == UserRole.Shipper)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();
        }
        public async Task<int> CountShipperAsync()
        {
            return await _context.Users.Where(p => p.RecordStatus != EntityStatus.Deleted && p.UserRole == UserRole.Shipper).CountAsync();
        }

        public async Task<List<User>> GetAsyncByAdmin(int pageNum, int pageSize, string search)
        {
            string searchStr = "";
            if (search != null)
            {
                searchStr = StringUtil.RemoveSign4VietnameseString(search.ToLower());
            }
            var product = await _context.Users.Select(p => new { str = StringUtil.RemoveSign4VietnameseString(p.LastName.ToLower() + " " + p.FirstName.ToLower()), user = p }).ToListAsync();
            return product.Where(p => p.str.Contains(searchStr))
            .Select(p => p.user)
            .Skip((pageNum - 1) * pageSize)
            .Take(pageSize)
            .ToList();
        }
        public async Task<int> CountAsyncByAdmin(string search)
        {
            string searchStr = "";
            if (search != null)
            {
                searchStr = StringUtil.RemoveSign4VietnameseString(search.ToLower());
            }
            var product = await _context.Users.Select(p => new { str = StringUtil.RemoveSign4VietnameseString(p.LastName.ToLower() + " " + p.FirstName.ToLower()), user = p }).ToListAsync();
            return product.Where(p => p.str.Contains(searchStr)).Count();
        }

        public async Task<UserResponseModel> DeleteAsync(User user)
        {
            try
            {
                //_context.Users.Remove(user);
                user.RecordStatus = EntityStatus.Deleted;
                _context.Entry(user).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return new UserResponseModel
                {
                    IsSuccess = true,
                    Message = "Delete success",
                };
            }
            catch (Exception ex)
            {
                return new UserResponseModel
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
