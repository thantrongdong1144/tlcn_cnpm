﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TLCN_CNPM.Entity;

namespace TLCN_CNPM.Data
{
    public class MarketDBContext: DbContext
    {
        public MarketDBContext(DbContextOptions<MarketDBContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuDetail> MenuDetails { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<DiscountCode> DiscountCodes { get; set; }
        public DbSet<FavoriteProduct> FavoriteProducts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Category>().ToTable("Category");
            modelBuilder.Entity<Product>().ToTable("Product");
            modelBuilder.Entity<Order>().ToTable("Orders");
            modelBuilder.Entity<OrderDetail>().ToTable("OrderDetail").HasKey(table => new
            {
                table.OrderId,
                table.ProductId
            });
            modelBuilder.Entity<Menu>().ToTable("Menus");
            modelBuilder.Entity<MenuDetail>().ToTable("MenuDetail").HasKey(table => new
            {
                table.MenuId,
                table.ProductId
            });
            modelBuilder.Entity<Comment>().ToTable("Comment");
            modelBuilder.Entity<DiscountCode>().ToTable("DiscountCode");
            modelBuilder.Entity<FavoriteProduct>().ToTable("FavoriteProduct").HasKey(table => new
            {
                table.UserId,
                table.ProductId
            });
        }
        public virtual void Commit()
        {
            base.SaveChanges();
        }
    }
}
