﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;

namespace TLCN_CNPM.Data
{
    public class DbInitializer
    {
        public static void Initialize(MarketDBContext context)
        {
            // Look for any students.
            if (!context.Users.Any())
            {
                var users = new User[]
                {
                    new User{
                        FirstName="Dong",
                        LastName="Than",
                        Username="admin@gmail.com",
                        Email="admin@gmail.com",
                        Phone="0337579124",
                        Sex="male",
                        UserRole=0,
                        Status= true,
                        PassWord="admin",
                        RegisterDate=DateTime.Parse("2021-09-02"),
                        Avatar = "https://res.cloudinary.com/donghcmute/image/upload/v1634690680/user_bbt8lk.png"
                    },
                    new User{
                        FirstName="Khoa",
                        LastName="Phan",
                        Username="khoaphan123",
                        Email="khoaphan123@gmail.com",
                        Phone="0399469372",
                        Sex="male",
                        UserRole=0,
                        Status= true,
                        PassWord="123456",
                        RegisterDate=DateTime.Parse("2021-09-02"),
                        Avatar = "https://res.cloudinary.com/donghcmute/image/upload/v1634690680/user_bbt8lk.png"
                    },
                    new User{
                        FirstName="Đạt",
                        LastName="Lê",
                        Username="datle123",
                        Email="datle123@gmail.com",
                        Phone="0399469371",
                        Sex="female",
                        UserRole=0,
                        Status= true,
                        PassWord="123456789",
                        RegisterDate=DateTime.Parse("2021-09-02"),
                        Avatar = "https://res.cloudinary.com/donghcmute/image/upload/v1634690680/user_bbt8lk.png"
                    },
                };

                context.Users.AddRange(users);
                context.SaveChanges();
            }
            if (!context.Categories.Any())
            {
                var categories = new Category[]
                {
                    new Category
                    {
                        Name = "Rau củ",
                        Icon = "fas fa-carrot"
                    },
                    new Category
                    {
                        Name = "Hoa quả",
                        Icon = "fas fa-apple-alt"
                    },
                    new Category
                    {
                        Name = "Bánh và trứng",
                        Icon = "fas fa-egg"
                    },
                    new Category
                    {
                        Name = "Thức ăn nhanh",
                        Icon = "fas fa-pizza-slice"
                    },
                    new Category
                    {
                        Name = "TP bảo vệ sức khỏe",
                        Icon = "fas fa-shield-virus"

                    },
                    new Category
                    {
                        Name = "TP cho trẻ em",
                        Icon = "fas fa-baby-carriage"
                    },
                    new Category
                    {
                        Name = "Đang giảm giá",
                        Icon = "fas fa-tags"
                    },
                };


                context.Categories.AddRange(categories);
                context.SaveChanges();
            }
            if (!context.Products.Any())
            {
                var products = new Product[]
                {
                    new Product
                    {
                        Code = "8936039968800",
                        Name = "Tỏi",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551755/1_to%CC%89i_ugfd4r.jpg",
                        Description = "Tỏi có thể sử dụng thành gia vị trong nước chấm pha chế gồm mắm, tỏi, ớt, tương, đường..." +
                        "Hoặc tỏi được trộn đều với các món rau xào (nhất là rau muống xào...) khiến món ăn dậy mùi thơm. " +
                        "Tỏi cũng được làm nước muối tỏi và ớt. Trong nấu ăn một số món có kèm theo tỏi phi.Phần hay được" +
                        " sử dụng nhất của cả cây tỏi là củ tỏi. Củ tỏi có nhiều tép. " +
                        "Từng tép tỏi cũng như cả củ tỏi đều có lớp vỏ mỏng bảo vệ. " +
                        "Tỏi sinh trưởng tốt trong môi trường nóng và ẩm. Nếu muốn bảo quản tỏi dùng trong nấu nướng, " +
                        "cần cất tỏi ở chỗ khô ráo thì sẽ không mọc mầm. Khi nấu nướng cần bỏ lớp vỏ bảo vệ và vứt bỏ phần " +
                        "mầm tỏi thường màu xanh có thể nằm sâu trong tép tỏi. Một số dân tộc trên thế giới tin rằng tỏi giúp" +
                        " họ chống lại ma, quỷ, ma cà rồng.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968801",
                        Name = "Rau Muống",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551755/1_rau_mu%C3%B4%CC%81ng_g0jnxe.png",
                        Description = "Từ rau muống, cách đơn giản nhất là luộc lên. Và tùy theo từng vùng, người ta có thể chấm với nước mắm, xì dầu, chao, mắm tép và tương (đặc biệt là tương Bần). Nước của rau muống luộc cũng thường được người dân Việt Nam uống pha với chanh sau bữa ăn. Tại Việt Nam xưa (làng Thanh Chiểu nay là Sen Chiểu - Phúc Thọ - Hà Nội) đã từng có loại rau muống được nuôi trồng rất cầu kỳ bằng cách cho ngọn rau mọc cuộn trong những chiếc vỏ ốc rỗng, để lấy những ngọn rau muống trắng nõn và mập mạp làm thức ăn....Ngoài rau muống luộc, còn có rau muống xào tỏi (có thể gia chút mắm tôm theo truyền thống); làm nộm rau muống với lạc rang giã dập, giấm, đường, tỏi, ớt; gia vào canh riêu cua hoặc canh cua khoai sọ thay cho rau rút, ăn với lẩu gà, làm rau muống nướng. Cũng thường thấy rau muống được chẻ ra ăn sống với các loại rau thơm khác. Mỗi cách đều có hương vị riêng và tùy sở thích của từng vùng, từng miền mà cách chế biến có khác nhau. Khuyến cáo: rau muống ăn sống phải được rửa kỹ, ngâm nước muối hoặc nước ozone để khử trùng.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968802",
                        Name = "Súp lơ",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551755/1_su%CC%81p_l%C6%A1_cevzp4.jpg",
                        Description = "Người ta có thể dùng súp lơ để là món luộc ăn như rau nhưng thông dụng nhất là các món hầm, súp hoặc ăn sống hay ngâm giấm. Ở Việt Nam, súp lơ thường được dùng cho các món xào, canh.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968803",
                        Name = "Rau Thơm",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551755/1_rau_th%C6%A1m_hjrtnx.jpg",
                        Description = "Rau thơm có vị cay, tính ấm, không độc, tiêu thức ăn, trị phong tà, thông đại tiểu tiện, trị các chứng đậu, sởi khó mọc, phá mụn độc... Rau mùi được trồng phổ biến ở miền bắc và thường có trong mùa đông.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968804",
                        Name = "Rau chân vịt",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551755/1_rau_ch%C3%A2n_vi%CC%A3t_g8wqrk.jpg",
                        Description = "Rau chân vịt (cải bó xôi) là một thành viên của họ rau dền, đã từng được trồng ở Ba Tư từ cách đây hàng nghìn năm sau đó du nhập về Trung Quốc cách đây khoảng 1.500 năm. Vài trăm năm sau đó, loại rau này được mang đến châu Âu và nhanh chóng có mặt trong nhiều món ăn ngon.Rau chân vịt là một loại rau ăn lá giàu khoáng chất như kali, kẽm, magiê, sắt, canxi… cùng các loại vitamin như folate, niacin, vitamin A, vitamin B6, vitamin C, vitamin K, vitamin B1 (thiamine), vitamin B2 (riboflavin) và nhiều vitamin thiết yếu khác. Đây là một loại rau rất có lợi cho sức khỏe với hàm lượng chất béo thấp. Bạn có thể dùng để chế biến các món như: salad, xào hay nấu canh, chiên trứng…",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968805",
                        Name = "Rau Má",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551755/1_rau_ma%CC%81_lji2q0.jpg",
                        Description = "Rau má có tác dụng tiêu nhiệt, dưỡng âm, giải độc,… thường dùng để điều trị nhiều bệnh như viêm họng, viêm amidan, ngộ độc thực phẩm. Ngoài ra, chúng còn được sử dụng với mục đích giảm nhanh các triệu chứng bệnh ngoài da và hỗ trợ cải thiện bệnh tim mạch và thần kinh.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968806",
                        Name = "Rau cải cay",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551754/1_rau_ca%CC%89i_cay_iaa3py.jpg",
                        Description = "Trong rau cải xanh chứa nhiều các loại vitamin trong đó có vitamin A và vitamin C, một trong các loại vitamin giúp cải thiện hệ miễn dịch, nâng cao sự phát triển và phân phối của tế bào T - tế bào bạch cầu cần thiết để giúp chống lại các bệnh nhiễm trùng tiềm ẩn.Chỉ cần khoảng 56gr rau cải xanh sống hoặc 140gr rau cải xanh đã được làm chín là đã cung cấp hơn 1/3 nhu cầu cung cấp vitamin C cho một ngày.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968807",
                        Name = "Ớt chuông",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551754/1_%C6%A1%CC%81t_chu%C3%B4ng_susldm.jpg",
                        Description = "Ớt chuông có thành phần chất dinh dưỡng khá giàu gồm: vitamin A, vitamin C và các chất dinh dưỡng khác. Hàm lượng vitamin A trong 149 gam chuông xanh cung cấp khoảng 551 IU vitamin A, tương đương với một chén nhỏ. Tuy nhiên, hàm lượng vitamin A trong ớt chuông đỏ nhiều hơn và tốt hơn cho sự phát triển của thị lực và mắt. Sử dụng một chén ớt xắt nhỏ với nhiều màu sắc có thể cung cấp hơn 100% giá trị hàng ngày..",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968808",
                        Name = "Ngô",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551754/1_ng%C3%B4_wevvqs.jpg",
                        Description = "Bắp ngô cũng rất giàu beta-carotenoid và folate, đây là hai chất này giúp làm chậm quá trình suy thoái điểm vàng liên quan đến tuổi tác. Beta-carotenoid trong bắp ngô khi đi vào cơ thể sẽ chuyển thành vitamin A với tỷ lệ cao hơn so với những loại rau củ khác. Vitamin A rất cần thiết cho để có một đôi mắt sáng đẹp.Màu sắc khác nhau của ngô cũng đem lại những lợi ích sức khỏe khác nhau, chủ yếu là bởi vì chúng chứa các sắc tố không giống nhau. So với ngô trắng, ngô vàng có chứa beta-carotene và zeaxanthin, tốt cho việc duy trì sức khỏe thị lực.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968809",
                        Name = "Bắp Cải",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551749/1_b%C4%83%CC%81p_ca%CC%89i_qdvmdv.jpg",
                        Description = "Bề mặt lá bắp cải xanh trơn nhẵn, mọng nước. Chúng có màu đậm ở bên ngoài và nhạt dần ở các lớp trong. Lá bên ngoài thường có màu xanh lá đậm đến xanh lá nhạt. Lá ở bên trong có màu xanh nhạt đến màu trắng. Kết cấu giữa lớp lá không quá chặt, lá giòn và dễ gãy khi tách.Bắp cải xanh có nguồn gốc từ Đan Mạch. Chúng thường được thu hoạch vào cuối mùa thu. Giống cải bắp này hiện đang được trồng và phân phối phổ biển ở thị trường Việt Nam, có sắc xanh rất nhạt, gần như là màu trắng thường được gọi là bắp cải trắng.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968810",
                        Name = "Bí đao",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551749/1_bi%CC%81_%C4%91ao_sr7thg.jpg",
                        Description = "Bí đao còn gọi là bí xanh, bí phấn… là một loại quả dùng như rau tươi và làm mứt rất thông dụng. Trong thành phần của bí đao phần lớn là nước, không chứa lipid, với hàm lượng natri rất thấp. Cứ 100g bí đao có 0,4g protid, 2,4g glucid, 19 mg canxi, 12 mg photpho, 0,3 mg sắt và nhiều loại vitamin như Caroten, B1, B2, B3, C…",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968811",
                        Name = "Bí đỏ",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551750/1_bi%CC%81_%C4%91o%CC%89_p0itj3.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968812",
                        Name = "Cà tím",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551750/1_ca%CC%80_ti%CC%81m_zsbydq.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968813",
                        Name = "Củ cải",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551750/1_cu%CC%89_ca%CC%89i_jzk73r.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968814",
                        Name = "Củ đậu",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551750/1_cu%CC%89_%C4%91%C3%A2%CC%A3u_nkdkkr.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968815",
                        Name = "Bông cải xanh",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551750/1_b%C3%B4ng_ca%CC%89i_xanh_s7lna8.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968816",
                        Name = "Cà rốt",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551750/1_carot_awhasd.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968817",
                        Name = "Đậu Hà Lan",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551751/1_%C4%91%C3%A2%CC%A3u_ha%CC%80_lan_vwmlcx.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968818",
                        Name = "Củ dền",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551751/1_cu%CC%89_d%C3%AA%CC%80n_rmcdxn.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968819",
                        Name = "Dậu que",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551751/1_%C4%91%C3%A2%CC%A3u_que_hjzwbs.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968820",
                        Name = "Su Hào",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551751/1_cu%CC%89_su_ha%CC%80o_qhyq6s.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968821",
                        Name = "Gừng",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551751/1_g%C6%B0%CC%80ng_mipdbu.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968822",
                        Name = "Giá Đỗ",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551751/1_gia%CC%81_%C4%91%C3%B4%CC%83_vwtvzc.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968823",
                        Name = "Lá Lốt",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551751/1_la%CC%81_l%C3%B4%CC%81t_k8icbh.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968824",
                        Name = "Hành lá",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551752/1_ha%CC%80nh_la%CC%81_f98fxv.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968825",
                        Name = "Măng Tây",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551752/1_m%C4%83ng_t%C3%A2y_cy5jdf.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968826",
                        Name = "Mía",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551752/1_mi%CC%81a_eb0rko.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968827",
                        Name = "Mướp",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551752/1_m%C6%B0%C6%A1%CC%81p_emr4hi.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968828",
                        Name = "Nấm kim châm",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551752/1_n%C3%A2%CC%81m_kim_ch%C3%A2m_zymyc8.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968829",
                        Name = "Mướp đắng",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551752/1_m%C6%B0%C6%A1%CC%81p_%C4%91%C4%83%CC%81ng_sjs3vj.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968830",
                        Name = "Nấm đùi gà",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551752/1_n%C3%A2%CC%81m_%C4%91u%CC%80i_ga%CC%80_c8cf8o.png",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968831",
                        Name = "Nấm mỡ",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551753/1_n%C3%A2%CC%81m_m%C6%A1%CC%83_ji4z8h.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968832",
                        Name = "Nấm mộc nhĩ",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551753/1_n%C3%A2%CC%81m_m%C3%B4%CC%A3c_nhi%CC%83_gfkqkn.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968833",
                        Name = "Nấm mối",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551753/1_n%C3%A2%CC%81m_m%C3%B4%CC%83i_kfsb0l.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968834",
                        Name = "Dưa chuột",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551753/1_d%C6%B0a_chu%C3%B4%CC%A3t_qbbvxr.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968835",
                        Name = "Nấm rơm",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551754/1_n%C3%A2%CC%81m_r%C6%A1m_islxzu.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968836",
                        Name = "Nghệ",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551754/1_ngh%C3%AA%CC%A3_wkubsl.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968837",
                        Name = "Nấm sò",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551754/1_n%C3%A2%CC%81m_so%CC%80_hh8flr.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968838",
                        Name = "Nấm linh chi",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551754/1_n%C3%A2%CC%81m_linh_chi_q9gjrj.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968839",
                        Name = "Ớt cay",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631551754/1_%C6%A1%CC%81t_cay_eoeqme.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 1,
                    },
                    new Product
                    {
                        Code = "8936039968840",
                        Name = "Mận",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758335/2_m%C3%A2%CC%A3n_h8vb7t.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968841",
                        Name = "Bưởi",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758324/2_b%C6%B0%C6%A1%CC%89i_ialbhs.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968842",
                        Name = "Cam",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758326/2_cam_v7dinz.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968843",
                        Name = "Cà chua",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758326/2_cachua_zhz98f.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968844",
                        Name = "Chanh",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758326/2_chanh_aevwg1.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968845",
                        Name = "Dâu",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758326/2_d%C3%A2u_qwuqmp.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968846",
                        Name = "Đào",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758326/2_%C4%91a%CC%80o_kwntp1.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968847",
                        Name = "Chùm ruột",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758327/2_chu%CC%80m_ru%C3%B4%CC%A3t_aepz0l.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968848",
                        Name = "Khế",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758327/2_kh%C3%AA%CC%81_gghf1x.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968849",
                        Name = "Dừa",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758328/2_d%C6%B0%CC%80a_spgfga.png",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968850",
                        Name = "Lựu",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758328/2_luu_no8dhi.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968851",
                        Name = "Dưa hấu",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758328/2_d%C6%B0ahau_gel2sf.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968852",
                        Name = "Nho",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758328/2_nho_m9qj1x.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968853",
                        Name = "Nhãn",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758329/2_nha%CC%83n_qebtdz.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968854",
                        Name = "Nho Xanh",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758329/2_nhoxanh_ukmvtg.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968855",
                        Name = "Ôỉ",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758329/2_%C3%B4%CC%89i_au6y7c.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968856",
                        Name = "Cam Thái Lan",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758330/2_orage_ehgbx5.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968857",
                        Name = "Hạt Óc Chó",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758330/2_ochco_md8xnn.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968858",
                        Name = "Sampoche",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758330/2_sampoche_scr2bc.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968859",
                        Name = "Vải",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758331/2_va%CC%89i_gukmxd.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968860",
                        Name = "Táo",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758331/2_tao_x9xx2h.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968860",
                        Name = "Táo",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758331/2_tao_x9xx2h.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968861",
                        Name = "Thanh Long",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758331/2_thanhlong_rkyvys.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968862",
                        Name = "Xoài",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758331/2_xoai_l1hvac.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968863",
                        Name = "Mận",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758335/2_m%C3%A2%CC%A3n_h8vb7t.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 2,
                    },
                    new Product
                    {
                        Code = "8936039968864",
                        Name = "Bánh Bao",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758119/3_Ba%CC%81nh_bao_a2jcne.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968865",
                        Name = "Bánh Dâu",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758119/3_Ba%CC%81nh_d%C3%A2u_yp13dc.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968866",
                        Name = "Bánh Dưa Hấu",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758120/3_Ba%CC%81nh_d%C6%B0a_h%C3%A2%CC%81u_ndkqpt.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968867",
                        Name = "Bánh Bông Lan Phô Mai",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758122/3_Ba%CC%81nh_b%C3%B4ng_lan_ph%C3%B4_mai_shi3lf.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968868",
                        Name = "Bánh Matcha",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758123/3_Ba%CC%81nh_matcha_rjqwyj.png",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968869",
                        Name = "Bánh Tiramisu",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758124/3_Ba%CC%81nh_tiramisu_yatur3.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968870",
                        Name = "Bánh Tiramisu",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758124/3_Ba%CC%81nh_tiramisu_yatur3.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968871",
                        Name = "Bánh Socola",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758125/3_Ba%CC%81nh_socola_cnqo3i.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968872",
                        Name = "Bánh Maracon",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758127/3_Ba%CC%81nh_maracon_pgyuqo.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968873",
                        Name = "Bánh Trung Thu",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758127/3_Ba%CC%81nh_trung_thu_mxtwyk.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968874",
                        Name = "Trứng gà",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758127/tr%C6%B0%CC%81ng_ga%CC%80_e2oavf.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968875",
                        Name = "Trứng Vịt",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758128/tr%C6%B0%CC%81ng_vi%CC%A3t_fnlfmh.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968876",
                        Name = "Bánh Chesse",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758129/3_Ba%CC%81nh_chesse_fnvs53.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968877",
                        Name = "Bánh Táo",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758129/3_Ba%CC%81nh_ta%CC%81o_t9un7u.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968878",
                        Name = "Bánh Trifle",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758142/3_Ba%CC%81nh_trifle_ssrsnn.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968879",
                        Name = "Bánh Doraemon",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758146/3_Ba%CC%81nh_doremon_kzoppa.png",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968880",
                        Name = "Bánh Mì",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758148/ba%CC%81nh_mi%CC%80_wy3zgp.png",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 3,
                    },
                    new Product
                    {
                        Code = "8936039968881",
                        Name = "Kem Khoai Môn",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758206/4__kem_khoai_m%C3%B4n_h%C3%B4%CC%A3p_eh3bci.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968882",
                        Name = "Nước uống C2 đóng chai",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758206/4_c2_chai_ypqyoo.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968883",
                        Name = "Nước uống Cocacola đóng chai",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758206/4_cocacola_chai_teh4hw.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968884",
                        Name = "Đùi Gà Chiên",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758206/4_%C4%91u%CC%80i_ga%CC%80_chi%C3%AAn_mdsgz1.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968885",
                        Name = "7UP dạng lon",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758206/4_7up_lon_i2z1yd.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968886",
                        Name = "Cocacola dạng lon",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758206/4_cocacola_lon_nus3lj.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968887",
                        Name = "7UP dạng chai",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758207/4_7up_chai_r5pq3q.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968888",
                        Name = "Hotdog",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758208/4_hotdog_yubaee.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968889",
                        Name = "Hambeger",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758208/4_hambeger_vbiech.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968890",
                        Name = "Kem Chuối",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758209/4_kem_chu%C3%B4%CC%81i_wl6nw8.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968891",
                        Name = "Kem Đậu Xanh",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758209/4_kem_%C4%91%C3%A2%CC%A3u_xanh_h%C3%B4%CC%A3p_wkebm1.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968891",
                        Name = "Kem Đậu Xanh",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758209/4_kem_%C4%91%C3%A2%CC%A3u_xanh_h%C3%B4%CC%A3p_wkebm1.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968892",
                        Name = "Kem Dâu Cây",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758209/4_kem_d%C3%A2u_lr1d7t.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968893",
                        Name = "Kem Dâu Hộp",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758210/4_kem_d%C3%A2u_h%C3%B4%CC%A3p_mvinz6.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968894",
                        Name = "Kem Dừa",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758210/4_kem_d%C6%B0%CC%80a_h%C3%B4%CC%A3p_jiws6i.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968895",
                        Name = "Kem Socola",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758210/4_kem_socola_hpsafe.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968896",
                        Name = "Kem Vani",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758210/4_kem_vani_h%C3%B4%CC%A3p_y04gfs.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968897",
                        Name = "Khoai Tây Chiên",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758211/4_khoai_t%C3%A2y_chi%C3%AAn_znztbd.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968898",
                        Name = "Kem Socola Hộp",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758211/4_kem_socola_h%C3%B4%CC%A3p_xqiuaj.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968899",
                        Name = "Kem Sầu Riêng",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758211/4_kem_s%C3%A2%CC%80u_ri%C3%AAng_h%C3%B4%CC%A3p_kcmiow.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968901",
                        Name = "Pepsi Dạng Chai",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758212/4_pepsi_chai_hdcpmk.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968902",
                        Name = "Pepsi Dạng Lon",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758212/4_pepsi_lon_waasab.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968903",
                        Name = "Number1 Dạng Chai",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758212/4_number1_chai_aj1bb4.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968904",
                        Name = "Pizza Bò",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758212/4_pizza_bo%CC%80_cfua0x.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968905",
                        Name = "Pizza Hải Sản",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758212/4_pizza_ha%CC%89i_sa%CC%89n_a6c93l.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968906",
                        Name = "Pizza Mực",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758213/4_pizza_m%C6%B0%CC%A3c_zqfhho.png",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968907",
                        Name = "Pizza Phô Mai",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758213/4_pizza_ph%C3%B4_mai_m0liz8.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968908",
                        Name = "Pizza Tôm",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758213/4_pizza_t%C3%B4m_rj7olr.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968909",
                        Name = "Pizza Truyền Thống",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758213/4_pizza_truy%C3%AA%CC%80n_th%C3%B4%CC%81ng_acg0dl.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968910",
                        Name = "Pizza Rau Củ",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758214/4_pizza_rau_cu%CC%89_kqd3b1.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968911",
                        Name = "Pizza Xúc Xích",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758214/4_pizza_xu%CC%81c_xi%CC%81ch_rgs3z5.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product////////////////////////////////
                    {
                        Code = "8936039968912",
                        Name = "Mirinda Dạng Chai",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758213/4_mirinda_chai_qw5wgn.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968913",
                        Name = "Red Bull Dạng Lon",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758214/4_red_bull_kciz4i.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968914",
                        Name = "String Đỏ",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758215/4_sting_%C4%91o%CC%89_wolee2.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968915",
                        Name = "String Vàng",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758215/4_sting_va%CC%80ng_a0m67d.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                    new Product
                    {
                        Code = "8936039968916",
                        Name = "Wake-Up 247 Dạng Chai",
                        Picture = "https://res.cloudinary.com/donghcmute/image/upload/v1631758215/4_wk_247_giklkh.jpg",
                        Description = "Bí đỏ là một loại thực phẩm rất giàu dinh dưỡng, đặc biệt tốt cho sức khỏe. Mặc dù rất giàu vitamin và khoáng chất nhưng loại quả này lại chứa rất ít calo.",
                        Price = 20000,
                        Quantity = 2000,
                        Like = 200,
                        Star = 4,
                        CategoryId = 4,
                    },
                };

                context.Products.AddRange(products);
                context.SaveChanges();
            }

        }
    }
}
