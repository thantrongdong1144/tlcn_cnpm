﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Repositories;

namespace TLCN_CNPM.Data
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> CommitAsync();
        IUserRepository Users { get; }
        IProductRepository Products { get; }
        ICategoryRepository Categories { get; }
        IOrderRepository Orders { get; }
        IOrderDetailRepository OrderDetails { get; }
        IMenuRepository Menus { get; }
        IMenuDetailRepository MenuDetails { get; }
        ICommentRepository Comments { get; }
        IDiscountCodeRepository DiscountCodes { get; }
        IFavoriteProductRepository FavoriteProducts { get; }
        IDashBoardRepository DashBoards { get; }
    }
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MarketDBContext _context;
        private UserRepository _userRepository;
        private ProductRepository _productRepository;
        private CategoryRepository _categoryRepository;
        private OrderRepository _orderRepository;
        private OrderDetailRepository _orderDetailRepository;
        private MenuRepository _menuRepository;
        private MenuDetailRepository _menuDetailRepository;
        private CommentRepository _commentRepository;
        private DiscountCodeRepository _discountCodeReposity;
        private FavoriteProductRepository _favoriteProductReposity;
        private DashBoardRepository _dashBoardReposity;

        public UnitOfWork(MarketDBContext context)
        {
            _context = context;
        }
        public IUserRepository Users => _userRepository = _userRepository ?? new UserRepository(_context);
        public IProductRepository Products => _productRepository = _productRepository ?? new ProductRepository(_context);
        public ICategoryRepository Categories => _categoryRepository = _categoryRepository ?? new CategoryRepository(_context);
        public IOrderRepository Orders => _orderRepository = _orderRepository ?? new OrderRepository(_context);
        public IOrderDetailRepository OrderDetails => _orderDetailRepository = _orderDetailRepository ?? new OrderDetailRepository(_context);
        public IMenuRepository Menus => _menuRepository = _menuRepository ?? new MenuRepository(_context);
        public IMenuDetailRepository MenuDetails => _menuDetailRepository = _menuDetailRepository ?? new MenuDetailRepository(_context);
        public ICommentRepository Comments => _commentRepository = _commentRepository ?? new CommentRepository(_context);
        public IDiscountCodeRepository DiscountCodes => _discountCodeReposity = _discountCodeReposity ?? new DiscountCodeRepository(_context);
        public IFavoriteProductRepository FavoriteProducts => _favoriteProductReposity = _favoriteProductReposity ?? new FavoriteProductRepository(_context);
        public IDashBoardRepository DashBoards => _dashBoardReposity = _dashBoardReposity ?? new DashBoardRepository(_context);


        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
