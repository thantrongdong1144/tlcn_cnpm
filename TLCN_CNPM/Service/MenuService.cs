﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;
using TLCN_CNPM.Data;

namespace TLCN_CNPM.Service
{
    public interface IMenuService
    {
        Task<MenuRepositoryModel> AddMenu(Menu menu);
        Task<List<Menu>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        Task<MenuRepositoryModel> DeleteAsync(Menu menu);
        Task<MenuRepositoryModel> UpdateAsync(Menu menu);
        Task<Menu> GetByIdAsync(int id);
        
    }
    public class MenuService : BaseService, IMenuService
    {
        public MenuService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public async Task<MenuRepositoryModel> AddMenu(Menu menu)
        {
            return await _unitOfWork.Menus.AddMenu(menu);
        }

        public async Task<int> CountAsync()
        {
            return await _unitOfWork.Menus.CountAsync();
        }

        public async Task<MenuRepositoryModel> DeleteAsync(Menu menu)
        {
            return await _unitOfWork.Menus.DeleteAsync(menu);
        }

        public async Task<List<Menu>> GetAsync(int pageNum, int pageSize)
        {
            return await _unitOfWork.Menus.GetAsync(pageNum, pageSize);
        }

        public async Task<Menu> GetByIdAsync(int id)
        {
            return await _unitOfWork.Menus.GetByIdAsync(id);
        }

        public async Task<MenuRepositoryModel> UpdateAsync(Menu menu)
        {
            return await _unitOfWork.Menus.UpdateAsync(menu);
        }
    }
}
