﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Data;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Service
{
    public interface IUserService
    {
        Task<User> GetByIdAsync(int id);
        Task<List<User>> GetAllAsync();
        Task<List<User>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        Task<List<User>> GetShipperAsync(int pageNum, int pageSize);
        Task<int> CountShipperAsync();
        Task<List<User>> GetAsyncByAdmin(int pageNum, int pageSize, string search);
        Task<int> CountAsyncByAdmin(string search);
        Task<UserResponseModel> AddAsync(User cate);
        Task<UserResponseModel> UpdateAsync(User cate);
        Task<UserResponseModel> DeleteAsync(User cate);
    }
    public class UserService: BaseService, IUserService
    {
        public UserService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public async Task<User> GetByIdAsync(int id)
        {
            return await _unitOfWork.Users.GetByIdAsync(id);
        }

        public async Task<List<User>> GetAllAsync()
        {
            return await _unitOfWork.Users.GetAllAsync();
        }

        public async Task<List<User>> GetAsync(int pageNum, int pageSize)
        {
            return await _unitOfWork.Users.GetAsync(pageNum, pageSize);
        }
        public async Task<int> CountAsync()
        {
            return await _unitOfWork.Users.CountAsync();
        }
        public async Task<List<User>> GetShipperAsync(int pageNum, int pageSize)
        {
            return await _unitOfWork.Users.GetShipperAsync(pageNum, pageSize);
        }
        public async Task<int> CountShipperAsync()
        {
            return await _unitOfWork.Users.CountShipperAsync();
        }

        public async Task<List<User>> GetAsyncByAdmin(int pageNum, int pageSize, string search)
        {
            return await _unitOfWork.Users.GetAsyncByAdmin(pageNum, pageSize, search);
        }
        public async Task<int> CountAsyncByAdmin(string search)
        {
            return await _unitOfWork.Users.CountAsyncByAdmin(search);
        }
        public async Task<UserResponseModel> AddAsync(User user)
        {
            return await _unitOfWork.Users.AddUserAsync(user);
        }
        public async Task<UserResponseModel> UpdateAsync(User user)
        {
            return await _unitOfWork.Users.UpdateUser(user);
        }

        public async Task<UserResponseModel> DeleteAsync(User cate)
        {
            return await _unitOfWork.Users.DeleteAsync(cate);
        }
    }
}
