﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;
using TLCN_CNPM.Data;

namespace TLCN_CNPM.Service
{
    public interface IOrderDetailService
    {
        Task<OrderDetailResponseModel> UpdateAsync(OrderDetail orderDetail);
        Task<OrderDetailResponseModel> DeleteAsync(OrderDetail orderDetail);
        Task<OrderDetailResponseModel> AddOrderDetail(OrderDetail orderDetail);
        Task<OrderDetail> GetByOrderIdAndProductId(int orderId, int productId);
        Task<List<OrderDetail>> GetListOrderDetailsByOrderId(int id);
    }
    public class OrderDetailService: BaseService, IOrderDetailService
    {
        public OrderDetailService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public async Task<OrderDetailResponseModel> AddOrderDetail(OrderDetail orderDetail)
        {
            return await _unitOfWork.OrderDetails.AddOrderDetail(orderDetail);
        }
        public async Task<OrderDetailResponseModel> UpdateAsync(OrderDetail orderDetail)
        {
            return await _unitOfWork.OrderDetails.UpdateAsync(orderDetail);
        }
        public async Task<OrderDetailResponseModel> DeleteAsync(OrderDetail orderDetail)
        {
            return await _unitOfWork.OrderDetails.DeleteAsync(orderDetail);
        }
        public async Task<OrderDetail> GetByOrderIdAndProductId(int orderId, int productId)
        {
            return await _unitOfWork.OrderDetails.GetByOrderIdAndProductId(orderId, productId);
        }
        public async Task<List<OrderDetail>> GetListOrderDetailsByOrderId(int id)
        {
            return await _unitOfWork.OrderDetails.GetListOrderDetailsByOrderId(id);
        }
    }
}
