﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;
using TLCN_CNPM.Data;

namespace TLCN_CNPM.Service
{
    public interface IOrderService
    {
        Task<OrderResponseModel> AddOrder(Order order);
        Task<List<Order>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        int CountAsyncByStatusAndShipperId(OrderStatus status, int Id);
        Task<List<Order>> GetAsyncByAdmin(int pageNum, int pageSize, string search);
        Task<List<Order>> GetAsyncByStatus(int pageNum, int pageSize, OrderStatus status);
        Task<int> CountAsyncByAdmin(string search);
        Task<int> CountAsyncByStatus(OrderStatus status);
        Task<OrderResponseModel> DeleteAsync(Order order);
        Task<OrderResponseModel> UpdateAsync(Order order);
        Task<Order> GetByIdAsync(int id);
        Task<List<Order>> GetListOrderForShipper(int shipperId);
        Task<List<Order>> GetListOrderForUser(int userId);
    }
    public class OrderService: BaseService, IOrderService
    {
        public OrderService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public async Task<OrderResponseModel> AddOrder(Order order)
        {
            return await _unitOfWork.Orders.AddOrder(order);
        }
        public async Task<List<Order>> GetAsync(int pageNum, int pageSize)
        {
            return await _unitOfWork.Orders.GetAsync(pageNum, pageSize);
        }
        public async Task<int> CountAsync()
        {
            return await _unitOfWork.Orders.CountAsync();
        }
        public int CountAsyncByStatusAndShipperId(OrderStatus status, int Id)
        {
            return _unitOfWork.Orders.CountAsyncByStatusAndShipperId(status, Id);
        }
        public async Task<List<Order>> GetAsyncByAdmin(int pageNum, int pageSize, string search)
        {
            return await _unitOfWork.Orders.GetAsyncByAdmin(pageNum, pageSize, search);
        }
        public async Task<List<Order>> GetAsyncByStatus(int pageNum, int pageSize, OrderStatus status)
        {
            return await _unitOfWork.Orders.GetAsyncByStatus(pageNum, pageSize, status);
        }
        public async Task<int> CountAsyncByAdmin(string search)
        {
            return await _unitOfWork.Orders.CountAsyncByAdmin(search);
        }
        public async Task<int> CountAsyncByStatus(OrderStatus status)
        {
            return await _unitOfWork.Orders.CountAsyncByStatus(status);
        }

        public async Task<OrderResponseModel> DeleteAsync(Order order)
        {
            return await _unitOfWork.Orders.DeleteAsync(order);
        }

        public async Task<Order> GetByIdAsync(int id)
        {
            return await _unitOfWork.Orders.GetByIdAsync(id);
        }
        public async Task<OrderResponseModel> UpdateAsync(Order order)
        {
            return await _unitOfWork.Orders.UpdateAsync(order);
        }
        public async Task<List<Order>> GetListOrderForShipper(int shipperId)
        {
            return await _unitOfWork.Orders.GetListOrderForShipper(shipperId);
        }

        public async Task<List<Order>> GetListOrderForUser(int userId)
        {
            return await _unitOfWork.Orders.GetListOrderForUser(userId);
        }
    }
}
