﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;
using TLCN_CNPM.Data;


namespace TLCN_CNPM.Service
{
    public interface IMenuDetailService
    {
        Task<MenuDetailRepositoryModel> AddMenuDetail(MenuDetail menuDetail);
        Task<MenuDetailRepositoryModel> UpdateAsync(MenuDetail menuDetail);
        Task<MenuDetailRepositoryModel> DeleteAsync(MenuDetail menuDetail);
        Task<MenuDetail> GetByMenuIdAndProductId(int menuId, int productId);
        Task<List<MenuDetail>> GetListMenuDetailsByMenuId(int id);
    }
    public class MenuDetailService : BaseService, IMenuDetailService
    {
        public MenuDetailService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public async Task<MenuDetailRepositoryModel> AddMenuDetail(MenuDetail menuDetail)
        {
            return await _unitOfWork.MenuDetails.AddMenuDetail(menuDetail);
        }

        public async Task<MenuDetailRepositoryModel> DeleteAsync(MenuDetail menuDetail)
        {
            return await _unitOfWork.MenuDetails.DeleteAsync(menuDetail);
        }

        public async Task<MenuDetail> GetByMenuIdAndProductId(int menuId, int productId)
        {
            return await _unitOfWork.MenuDetails.GetByMenuIdAndProductId(menuId, productId);
        }

        public async Task<List<MenuDetail>> GetListMenuDetailsByMenuId(int id)
        {
            return await _unitOfWork.MenuDetails.GetListMenuDetailsByMenuId(id);
        }

        public async Task<MenuDetailRepositoryModel> UpdateAsync(MenuDetail menuDetail)
        {
            return await _unitOfWork.MenuDetails.UpdateAsync(menuDetail);
        }
    }
}
