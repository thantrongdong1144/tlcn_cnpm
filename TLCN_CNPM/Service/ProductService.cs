﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Data;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Service
{
    public interface IProductService
    {
        Task<Product> GetByIdAsync(int id);
        Task<List<Product>> GetByCategoryIdAsync(int id, int pageNum, int pageSize);
        Task<List<Product>> GetByProductKeyAsync(string key, int pageNum, int pageSize);
        Task<List<Product>> GetAllAsync();
        Task<List<Product>> GetAsync(int pageNum, int pageSize);
        Task<List<Product>> GetProductsByAdmin(int pageNum, int pageSize, string search);
        Task<int> CountAsyncByAdmin(string search);
        Task<List<Product>> GetProductsByDiscount(int pageNum, int pageSize, int isDiscount);
        Task<int> CountAsyncByDiscount(int isDiscount);

        Task<int> CountAsync();
        Task<int> CountAsyncByCategoryId(int id);
        Task<int> CountAsyncByProductKey(string key);
        Task<List<Product>> GetPopularProduct();
        Task<List<Product>> GetNewProduct();
        Task<List<Product>> GetPopularProductLastWeek();
        Task<ProductResponseModel> AddProduct(Product prod);
        Task<ProductResponseModel> UpdateAsync(Product prod);
        Task<ProductResponseModel> DeleteAsync(Product prod);
    }
    public class ProductService: BaseService, IProductService
    {
        public ProductService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public async Task<Product> GetByIdAsync(int id)
        {
            return await _unitOfWork.Products.GetByIdAsync(id);
        }
        public async Task<List<Product>> GetByCategoryIdAsync(int id, int pageNum, int pageSize) 
        {
            return await _unitOfWork.Products.GetByCategoryIdAsync(id, pageNum, pageSize);
        }
        public async Task<List<Product>> GetByProductKeyAsync(string key, int pageNum, int pageSize)
        {
            return await _unitOfWork.Products.GetByProductKeyAsync(key, pageNum, pageSize);
        }
        public async Task<List<Product>> GetAllAsync()
        {
            return await _unitOfWork.Products.GetAllAsync();
        }
        public async Task<List<Product>> GetAsync(int pageNum, int pageSize)
        {
            return await _unitOfWork.Products.GetAsync(pageNum,pageSize);
        }
        public async Task<List<Product>> GetProductsByAdmin(int pageNum, int pageSize, string search)
        {
            return await _unitOfWork.Products.GetProductsByAdmin(pageNum, pageSize, search);
        }
        public async Task<int> CountAsyncByAdmin(string search)
        {
            return await _unitOfWork.Products.CountAsyncByAdmin(search);
        }
        public async Task<List<Product>> GetProductsByDiscount(int pageNum, int pageSize, int isDiscount)
        {
            return await _unitOfWork.Products.GetProductsByDiscount(pageNum, pageSize, isDiscount);
        }
        public async Task<int> CountAsyncByDiscount(int isDiscount)
        {
            return await _unitOfWork.Products.CountAsyncByDiscount(isDiscount);
        }
        public async Task<int> CountAsync()
        {
            return await _unitOfWork.Products.CountAsync();
        }
        public async Task<int> CountAsyncByCategoryId(int id)
        {
            return await _unitOfWork.Products.CountAsyncByCategoryId( id);
        }
        public async Task<int> CountAsyncByProductKey(string key)
        {
            return await _unitOfWork.Products.CountAsyncByProductKey(key);
        }
        public async Task<List<Product>> GetPopularProduct()
        {
            return await _unitOfWork.Products.GetPopularProduct();
        }
        public async Task<List<Product>> GetNewProduct()
        {
            return await _unitOfWork.Products.GetNewProduct();
        }
        public async Task<List<Product>> GetPopularProductLastWeek()
        {
            return await _unitOfWork.Products.GetPopularProductLastWeek();
        }
        public async Task<ProductResponseModel> AddProduct(Product prod)
        {
            return await _unitOfWork.Products.AddAsync(prod);
        }

        public async Task<ProductResponseModel> UpdateAsync(Product prod)
        {
            return await _unitOfWork.Products.UpdateAsync(prod);
        }

        public async Task<ProductResponseModel> DeleteAsync(Product prod)
        {
            return await _unitOfWork.Products.DeleteAsync(prod);
        }
    }
}
