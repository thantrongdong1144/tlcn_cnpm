﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Service
{
    public interface IFavoriteProductService
    {
        Task<List<FavoriteProduct>> GetListFavoriteProductByUserId(int id);
        Task<FavoriteProduct> GetFavoriteProduct(int userId, int productId);
        Task<FavoriteProductResponseModel> AddAsync(FavoriteProduct prod);
        Task<FavoriteProductResponseModel> UpdateAsync(FavoriteProduct prod);
        Task<FavoriteProductResponseModel> DeleteAsync(FavoriteProduct prod);
    }
    public class FavoriteProductService : BaseService, IFavoriteProductService
    {
        public FavoriteProductService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            
        }
        public async Task<List<FavoriteProduct>> GetListFavoriteProductByUserId(int id)
        {
            return await _unitOfWork.FavoriteProducts.GetListFavoriteProductByUserId(id);
        }
        public async Task<FavoriteProduct> GetFavoriteProduct(int userId, int productId)
        {
            return await _unitOfWork.FavoriteProducts.GetFavoriteProduct(userId, productId);
        }
        public async Task<FavoriteProductResponseModel> AddAsync(FavoriteProduct prod)
        {
            return await _unitOfWork.FavoriteProducts.AddAsync(prod);
        }
        public async Task<FavoriteProductResponseModel> UpdateAsync(FavoriteProduct prod)
        {
            return await _unitOfWork.FavoriteProducts.UpdateAsync(prod);
        }
        public async Task<FavoriteProductResponseModel> DeleteAsync(FavoriteProduct prod)
        {
            return await _unitOfWork.FavoriteProducts.DeleteAsync(prod);
        }
    }
}
