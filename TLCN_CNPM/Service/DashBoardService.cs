﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Data;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Service
{
    public interface IDashBoardService
    {
        Task<IEnumerable<RevenueModel>> GetRevenueOrderByMonth();
        Task<IEnumerable<RevenueModel>> GetRejectTotalOrderByMonth();
        Task<IEnumerable<RevenueModel>> GetNumOfOrderByMonth();
        Task<IEnumerable<dynamic>> GetProductTotalOrderByCategory();
        Task<IEnumerable<dynamic>> TotalNumberOfSuccessOrder();
        Task<IEnumerable<dynamic>> TotalRevenue();
        Task<IEnumerable<dynamic>> TotalUser();
        Task<IEnumerable<dynamic>> TotalRejectOrder();
    }
    public class DashBoardService : BaseService, IDashBoardService
    {
        public DashBoardService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public async Task<IEnumerable<RevenueModel>> GetRevenueOrderByMonth()
        {
            return await _unitOfWork.DashBoards.GetRevenueOrderByMonth();
        }
        public async Task<IEnumerable<RevenueModel>> GetRejectTotalOrderByMonth()
        {
            return await _unitOfWork.DashBoards.GetRejectTotalOrderByMonth();
        }
        public async Task<IEnumerable<RevenueModel>> GetNumOfOrderByMonth()
        {
            return await _unitOfWork.DashBoards.GetNumOfOrderByMonth();
        }
        public async Task<IEnumerable<dynamic>> GetProductTotalOrderByCategory()
        {
            return await _unitOfWork.DashBoards.GetProductTotalOrderByCategory();
        }
        public async Task<IEnumerable<dynamic>> TotalNumberOfSuccessOrder()
        {
            return await _unitOfWork.DashBoards.TotalNumberOfSuccessOrder();
        }
        public async Task<IEnumerable<dynamic>> TotalRevenue()
        {
            return await _unitOfWork.DashBoards.TotalRevenue();
        }
        public async Task<IEnumerable<dynamic>> TotalUser()
        {
            return await _unitOfWork.DashBoards.TotalUser();
        }
        public async Task<IEnumerable<dynamic>> TotalRejectOrder()
        {
            return await _unitOfWork.DashBoards.TotalRejectOrder();
        }
    }
}
