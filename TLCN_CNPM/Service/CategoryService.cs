﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Data;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Service
{
    public interface ICategoryService
    {
        Task<Category> GetByIdAsync(int id);
        Task<List<Category>> GetAllAsync();
        Task<List<Category>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        Task<List<Category>> GetAsyncByAdmin(int pageNum, int pageSize, string search);
        Task<int> CountAsyncByAdmin(string search);
        Task<CategoryResponseModel> AddAsync(Category cate);
        Task<CategoryResponseModel> UpdateAsync(Category cate);
        Task<CategoryResponseModel> DeleteAsync(Category cate);
        Task<List<CategoryWithQuantity>> GetCategoryWithTotalProduct();
    }
    public class CategoryService: BaseService, ICategoryService
    {
        public CategoryService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public async Task<Category> GetByIdAsync(int id)
        {
            return await _unitOfWork.Categories.GetByIdAsync(id);
        }

        public async Task<List<Category>> GetAllAsync()
        {
            return await _unitOfWork.Categories.GetAllAsync();
        }
        public async Task<List<Category>> GetAsync(int pageNum, int pageSize)
        {
            return await _unitOfWork.Categories.GetAsync(pageNum, pageSize);
        }
        public async Task<int> CountAsync()
        {
            return await _unitOfWork.Categories.CountAsync();
        }
        public async Task<List<Category>> GetAsyncByAdmin(int pageNum, int pageSize, string search)
        {
            return await _unitOfWork.Categories.GetAsyncByAdmin(pageNum, pageSize, search);
        }
        public async Task<int> CountAsyncByAdmin(string search)
        {
            return await _unitOfWork.Categories.CountAsyncByAdmin(search);
        }
        public async Task<CategoryResponseModel> AddAsync(Category cate)
        {
            return await _unitOfWork.Categories.AddAsync(cate);
        }
        public async Task<CategoryResponseModel> UpdateAsync(Category cate)
        {
            return await _unitOfWork.Categories.UpdateAsync(cate);
        }
        public async Task<CategoryResponseModel> DeleteAsync(Category cate)
        {
            return await _unitOfWork.Categories.DeleteAsync(cate);
        }
        public async Task<List<CategoryWithQuantity>> GetCategoryWithTotalProduct()
        {
            return await _unitOfWork.Categories.GetCategoryWithTotalProduct();
        }
    }
}
