﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;
using TLCN_CNPM.Data;

namespace TLCN_CNPM.Service
{
    public interface ICommentService
    {
        Task<CommentResponseModel> AddComment(Comment comment);
        Task<List<Comment>> GetCommentByProductId(int id, int pageNum, int pageSize);
        Task<int> CoutByProductIdAsync(int id);
        Task<Comment> GetByIdAsync(int id);
        Task<List<Comment>> GetAllAsync();
        Task<List<Comment>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
        Task<CommentResponseModel> UpdateAsync(Comment cmt);
        Task<CommentResponseModel> DeleteAsync(Comment cmt);
    }
    public class CommentService : BaseService, ICommentService
    {
        public CommentService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public async Task<CommentResponseModel> AddComment(Comment comment)
        {
            return await _unitOfWork.Comments.AddComment(comment);
        }
        public async Task<List<Comment>> GetCommentByProductId(int id, int pageNum, int pageSize)
        {
            return await _unitOfWork.Comments.GetCommentByProductId(id, pageNum, pageSize);
        }
        public async Task<int> CoutByProductIdAsync(int id)
        {
            return await _unitOfWork.Comments.CoutByProductIdAsync(id);
        }

        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _unitOfWork.Comments.GetByIdAsync(id);
        }

        public async Task<List<Comment>> GetAllAsync()
        {
            return await _unitOfWork.Comments.GetAllAsync();
        }

        public async Task<List<Comment>> GetAsync(int pageNum, int pageSize)
        {
            return await _unitOfWork.Comments.GetAsync(pageNum, pageSize);
        }

        public async Task<int> CountAsync()
        {
            return await _unitOfWork.Comments.CountAsync();
        }


        public async Task<CommentResponseModel> UpdateAsync(Comment cmt)
        {
            return await _unitOfWork.Comments.UpdateComment(cmt);
        }

        public async Task<CommentResponseModel> DeleteAsync(Comment cmt)
        {
            return await _unitOfWork.Comments.DeleteAsync(cmt);
        }
    }
}
