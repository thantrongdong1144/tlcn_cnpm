﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Service
{
    public interface IAuthService
    {
        Task<UserResponseModel> LoginAsync(LoginViewModel loginView);
        Task<UserResponseModel> UpdateUser(User user);
        Task<UserResponseModel> AddUserAsync(User user);
        Task<User> GetByIdAsync(int id);
        Task<User> GetByEmailAsync(string email);
    }
    public class AuthService : BaseService, IAuthService
    {
        public AuthService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        public async Task<UserResponseModel> LoginAsync(LoginViewModel loginView)
        {
            return await _unitOfWork.Users.LoginAsync(loginView);
        }
        public async Task<UserResponseModel> UpdateUser(User user)
        {
            return await _unitOfWork.Users.UpdateUser(user);
        }
        public async Task<User> GetByIdAsync(int id)
        {
            return await _unitOfWork.Users.GetByIdAsync(id);
        }
        public async Task<User> GetByEmailAsync(string email)
        {
            return await _unitOfWork.Users.GetByEmailAsync(email);
        }
        public async Task<UserResponseModel> AddUserAsync(User user)
        {
            return await _unitOfWork.Users.AddUserAsync(user);
        }
    }
}
