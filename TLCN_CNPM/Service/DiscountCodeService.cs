﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLCN_CNPM.Data;
using TLCN_CNPM.Entity;
using TLCN_CNPM.Model;

namespace TLCN_CNPM.Service
{
    public interface IDiscountCodeService
    {
        Task<List<DiscountCode>> GetDiscountCodeByUserId(int id);
        Task<DiscountCode> GetDiscountCodeByCode(string code);
        Task<DiscountCodeResponseModel> UpdateAsync(DiscountCode code);
        Task<DiscountCodeResponseModel> AddDiscountCode(DiscountCode code);
        Task<DiscountCodeResponseModel> DeleteAsync(DiscountCode code);

        Task<DiscountCode> GetByIdAsync(int id);
        Task<List<DiscountCode>> GetAllAsync();
        Task<List<DiscountCode>> GetAsync(int pageNum, int pageSize);
        Task<int> CountAsync();
    }
    public class DiscountCodeService: BaseService, IDiscountCodeService
    {
        public DiscountCodeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            
        }
        public async Task<List<DiscountCode>> GetDiscountCodeByUserId(int id)
        {
            return await _unitOfWork.DiscountCodes.GetDiscountCodeByUserId(id);
        }
        public async Task<DiscountCode> GetDiscountCodeByCode(string code)
        {
            return await _unitOfWork.DiscountCodes.GetDiscountCodeByCode(code);
        }
        public async Task<DiscountCodeResponseModel> UpdateAsync(DiscountCode code)
        {
            return await _unitOfWork.DiscountCodes.UpdateAsync(code);
        }

        public async Task<DiscountCodeResponseModel> AddDiscountCode(DiscountCode code)
        {
            return await _unitOfWork.DiscountCodes.AddDiscountCode(code);
        }

        public async Task<DiscountCodeResponseModel> DeleteAsync(DiscountCode code)
        {
            return await _unitOfWork.DiscountCodes.DeleteAsync(code);
        }

        public async Task<DiscountCode> GetByIdAsync(int id)
        {
            return await _unitOfWork.DiscountCodes.GetByIdAsync(id);
        }

        public async Task<List<DiscountCode>> GetAllAsync()
        {
            return await _unitOfWork.DiscountCodes.GetAllAsync();
        }

        public async Task<List<DiscountCode>> GetAsync(int pageNum, int pageSize)
        {
            return await _unitOfWork.DiscountCodes.GetAsync(pageNum, pageSize);
        }

        public async Task<int> CountAsync()
        {
            return await _unitOfWork.DiscountCodes.CountAsync();
        }
    }
}
