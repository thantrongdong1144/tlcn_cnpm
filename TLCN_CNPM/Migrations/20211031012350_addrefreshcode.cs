﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TLCN_CNPM.Migrations
{
    public partial class addrefreshcode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RefreshCode",
                table: "User",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RefreshCode",
                table: "User");
        }
    }
}
