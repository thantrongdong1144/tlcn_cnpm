﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TLCN_CNPM.Migrations
{
    public partial class addcharge : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ChargeArea",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChargeArea",
                table: "Users");
        }
    }
}
