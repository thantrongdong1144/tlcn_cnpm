﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TLCN_CNPM.Migrations
{
    public partial class addstatusentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Status",
                table: "Product",
                newName: "RecordStatus");

            migrationBuilder.AddColumn<int>(
                name: "RecordStatus",
                table: "Users",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RecordStatus",
                table: "Orders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RecordStatus",
                table: "Category",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RecordStatus",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "RecordStatus",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "RecordStatus",
                table: "Category");

            migrationBuilder.RenameColumn(
                name: "RecordStatus",
                table: "Product",
                newName: "Status");
        }
    }
}
