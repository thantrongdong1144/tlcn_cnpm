﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TLCN_CNPM.Migrations
{
    public partial class removeRelatedUserIndiscountcode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DiscountCode_User_UserId",
                table: "DiscountCode");

            migrationBuilder.DropIndex(
                name: "IX_DiscountCode_UserId",
                table: "DiscountCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_DiscountCode_UserId",
                table: "DiscountCode",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_DiscountCode_User_UserId",
                table: "DiscountCode",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
