﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Entity
{
    public class FavoriteProduct
    {
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int UserId { get; set; }
    }
}
