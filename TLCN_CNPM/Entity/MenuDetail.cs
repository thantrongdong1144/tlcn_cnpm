﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Entity
{
    public class MenuDetail
    {
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int MenuId { get; set; }
        public virtual Menu Menu { get; set; }
        public int TotalKalo { get; set; }
    }
}
