﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Entity
{
    public class Product
    {
        internal int ProductId;

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
        public float Calo { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public int Like { get; set; }
        public float Star { get; set; }
        public int DiscountPrice { get; set; }
        public DateTime ExpirationDiscountDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public EntityStatus RecordStatus { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
    public enum EntityStatus
    {
        NotDeleted,
        Deleted
    }
}
