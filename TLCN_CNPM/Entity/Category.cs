﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Entity
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Icon { get; set; }
        public EntityStatus RecordStatus { get; set; }

        //public virtual ICollection<Product> Products { get; set; }
    }
}
