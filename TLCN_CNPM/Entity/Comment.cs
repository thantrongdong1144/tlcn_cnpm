﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Entity
{
    public class Comment
    {
        public int Id { get; set; }
        public int Star { get; set; }
        public DateTime CommentDate { get; set; }
        public string CommentContent { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
