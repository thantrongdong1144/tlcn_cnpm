﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Entity
{
    public class Order
    {
        public int Id { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string ReceiverAddress { get; set; }
        public OrderStatus Status { get; set; }
        public PaymentType PaymentType { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Note { get; set; }
        public int RealTotalCost { get; set; }
        public bool IsApplyDiscount { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int ShipperId { get; set; }
        public EntityStatus RecordStatus { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails {get; set;}

        public static implicit operator Order(EntityEntry<Order> v)
        {
            throw new NotImplementedException();
        }
    }
    public enum OrderStatus
    {
        Processing,
        Approved,
        Delivering,
        Delivered,
        Reject,
        NotPay
    }
    public enum PaymentType
    {
        Cash,
        VNPay,
        All
    }
}
