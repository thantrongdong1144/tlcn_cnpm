﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Entity
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Phone { get; set; }
        public string Avatar { get; set; }
        public string Sex { get; set; }
        public string Email { get; set; }
        public UserRole UserRole { get; set; }
        public bool Status { get; set; }
        public string ChargeArea { get; set; }
        public string PassWord { get; set; }
        public string RefreshCode { get; set; }
        public DateTime RegisterDate { get; set; }
        public EntityStatus RecordStatus { get; set; }
    }
    public enum UserRole
    {
        Admin,
        Shipper,
        Customer
    }
}
