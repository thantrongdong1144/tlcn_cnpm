﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLCN_CNPM.Entity
{
    public class DiscountCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Desciption { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public PaymentType PaymentType { get; set; }
        public DiscountType DiscountType { get; set; }
        public int DiscountValue { get; set; }
        public int MinimumBill { get; set; }
        public bool IsUsed { get; set; }

        public int? UserId { get; set; }
        //public virtual User User { get; set; }
    }
    public enum DiscountType
    {
        PerCent,
        Cash
    }
}
