﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace TLCN_CNPM.Entity
{
    public class Menu
    {
        public int Id { get; set; }
        public string MenuName { get; set; }
        public int RealTotalKalo { get; set; }

        public string Picture { get; set; }

        public virtual ICollection<MenuDetail> MenuDetails { get; set; }

        public static implicit operator Menu(EntityEntry<Menu> v)
        {
            throw new NotImplementedException();
        }
    }
}
