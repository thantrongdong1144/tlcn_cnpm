import store from '@/store'

export default (to, from, next) => {
  let currentUser = store.getters['user/currentUser']

  if (!currentUser.id) {
    next('/login')
  } else {
    let role = currentUser.userRole
    if (role === 0) {
      next()
    } else {
      next('/')
    }
  }
}
