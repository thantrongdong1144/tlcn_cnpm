import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
// import Heloo from '@/components/TestComponent'
import Home from '@/views/clients/home/Home'
import Test from '@/components/Test.vue'
import Shop from '@/views/clients/shop/Shop.vue'
import Login from '@/views/clients/login/Login.vue'
import Cart from '@/views/clients/cart/Cart.vue'
import FavoriteProduct from '@/views/clients/favoriteProduct/FavoriteProduct.vue'
import Checkout from '@/views/clients/cart/Checkout.vue'
import Product from '@/views/clients/shop/Product.vue'
import Contact from '@/views/clients/contact/Contact.vue'
import AboutUs from '@/views/clients/aboutus/aboutus.vue'
import ClientLayout from '@/views/clients/ClientLayout.vue'

import Account from '@/views/clients/account/InfoAccount.vue'
import MyAccount from '@/views/clients/account/MyAccount.vue'
import DiscountCode from '@/views/clients/account/DiscountCode.vue'
import ChangePassword from '@/views/clients/account/ChangePassword.vue'
import ForgotPassword from '@/views/auth/ForgotPassword.vue'
import ConfirmPassword from '@/views/auth/ConfirmPassword.vue'
import Register from '@/views/auth/Register.vue'
import Menu from '@/views/clients/menu/Menu.vue'

// import InfoShipperAccount from '@/views/shipper/InfoShipperAccount.vue'
// import ChangePasswordShipper from '@/views/shipper/ChangePasswordShipper.vue'
import ShipperManageOrder from '@/views/shipper/OrderManagement/OrderLayout.vue'
import ListBill from '@/views/shipper/ListBill.vue'
// import DetailShipperBill from '@/views/shipper/DetailShipperBill.vue'

import AdminLayout from '../views/admin/AdminLayout.vue'
import AdminHome from '@/views/admin/home/AdminHome.vue'
import ProductLayout from '@/views/admin/productManagement/ProductLayout.vue'
import AccountLayout from '@/views/admin/accountManagement/AccountLayout.vue'
import CategoryLayout from '@/views/admin/categoryManagement/CategoryLayout.vue'
import CommentLayout from '@/views/admin/commentManagement/CommentLayout.vue'
import DiscountLayout from '@/views/admin/discountManagement/DiscountLayout.vue'
import OrderLayout from '@/views/admin/orderManagement/OrderLayout.vue'
import ShippingLayout from '@/views/admin/shippingManagemant/ShippingManagement.vue'
import ProductDiscountLayout from '@/views/admin/productDiscountManagement/productDiscountManagement.vue'
// import OrderDetailLayout from '@/views/admin/orderDetailManagement/OrderDetailLayout.vue'

import authGuard from './guard/auth.guard'

Vue.use(Router)
const homechildRoute = () => [
  {
    path: '',
    name: 'home-page',
    component: Home
  }
]
const shopchildRoute = () => [
  {
    path: '/shop',
    name: 'shop-page',
    component: Shop
  },
  {
    path: '/product',
    name: 'product-detail',
    component: Product
  }
]
const shipperRoute = () => [
  // {
  //   path: '/shipper',
  //   name: 'shipper-page',
  //   component: MyAccount
  // },
  // {
  //   path: '/shipperchangepass',
  //   name: 'shipper-changepass',
  //   component: ChangePasswordShipper
  // },
  {
    path: '/shipperlistbill',
    name: 'shipper-listbill',
    component: ListBill
  },
  // {
  //   path: '/shipperdetailbill',
  //   name: 'shipper-detailbill',
  //   component: DetailShipperBill
  // },
  {
    path: '/shipper-manage-order',
    name: 'shipperManageOrder-detailbill',
    component: ShipperManageOrder
  }
]
const contactchildRoute = () => [
  {
    path: '/contact',
    name: 'contact-page',
    component: Contact
  }
]
const aboutuschildRoute = () => [
  {
    path: '/aboutus',
    name: 'aboutus-page',
    component: AboutUs
  }
]

const loginchildRoute = () => [
  {
    path: '/login',
    name: 'login-page',
    component: Login
  }
]

const accountchildRoute = () => [
  {
    path: '/account',
    name: 'account-page',
    component: Account
  },
  {
    path: '/myaccount',
    name: 'myaccount-page',
    component: MyAccount
  },
  {
    path: '/changepassword',
    name: 'changepassword-page',
    component: ChangePassword
  },
  {
    path: '/discountcode',
    name: 'discountcode-page',
    component: DiscountCode
  },
  {
    path: '/forgotpassword',
    name: 'forgotpassword-page',
    component: ForgotPassword
  },
  {
    path: '/confirmpassword',
    name: 'confirmpassword-page',
    component: ConfirmPassword
  },
  {
    path: '/register',
    name: 'register-page',
    component: Register
  }
]

const cartchildRoute = () => [
  {
    path: '/cart',
    name: 'cart-page',
    component: Cart
  },
  {
    path: '/checkout',
    name: 'checkout-page',
    component: Checkout
  }
]

const favoriteProductchildRoute = () => [
  {
    path: '/favoriteProduct',
    name: 'favoriteProduct-page',
    component: FavoriteProduct
  }
]

const menuchildRoute = () => [
  {
    path: '/menu',
    name: 'menu-page',
    component: Menu
  }
]

const routes = [
  {
    path: '',
    name: 'home',
    component: ClientLayout,
    children: homechildRoute()
  },
  {
    path: '/shop',
    name: 'shop',
    component: ClientLayout,
    children: shopchildRoute()
  },
  {
    path: '/menu',
    name: 'menu',
    component: ClientLayout,
    children: menuchildRoute()
  },
  {
    path: '/contact',
    name: 'contact',
    component: ClientLayout,
    children: contactchildRoute()
  },
  {
    path: '/aboutus',
    name: 'aboutus',
    component: ClientLayout,
    children: aboutuschildRoute()
  },
  {
    path: '/account',
    name: 'account',
    component: ClientLayout,
    children: accountchildRoute()
  },
  {
    path: '/login',
    name: 'login',
    component: ClientLayout,
    children: loginchildRoute()
  },
  {
    path: '/cart',
    name: 'cart',
    component: ClientLayout,
    children: cartchildRoute()
  },
  {
    path: '/favoriteProduct',
    name: 'favoriteProduct',
    component: ClientLayout,
    children: favoriteProductchildRoute()
  },
  {
    path: '/test',
    name: 'test-page',
    component: Test
  },
  {
    path: '/shipper',
    name: 'shipper',
    component: ClientLayout,
    children: shipperRoute()
  },
  {
    path: '/admin',
    name: 'admin',
    component: AdminLayout,
    beforeEnter: authGuard,
    children: [
      {
        path: '/admin',
        name: 'adminhomelayout',
        component: AdminHome
      },
      {
        path: '/admin/product',
        name: 'productlayout',
        component: ProductLayout
      },
      {
        path: '/admin/account',
        name: 'accountlayout',
        component: AccountLayout
      },
      {
        path: '/admin/comment',
        name: 'commentLayout',
        component: CommentLayout
      },
      {
        path: '/admin/discount',
        name: 'discountlayout',
        component: DiscountLayout
      },
      {
        path: '/admin/order',
        name: 'orderlayout',
        component: OrderLayout
      },
      {
        path: '/admin/account',
        name: 'accountlayout',
        component: AccountLayout
      },
      {
        path: '/admin/category',
        name: 'categorylayout',
        component: CategoryLayout
      },
      {
        path: '/admin/shipping',
        name: 'shippingLayout',
        component: ShippingLayout
      },
      {
        path: '/admin/product-discount',
        name: 'productDiscountLayout',
        component: ProductDiscountLayout
      }
    ]
  }
]
const router = new Router({
  mode: 'history',
  base: '',
  routes
})

export default router
