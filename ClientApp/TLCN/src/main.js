// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
// import VueFbCustomerChat from 'vue-fb-customer-chat'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import './plugins'
import './assets/css/style.css'
import './assets/css/grid.css'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'
import GAuth from 'vue-google-oauth2'
import VueLoading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'
Vue.use(VueToast)
Vue.use(VueLoading)
// Vue.use(VueFbCustomerChat, {
//   page_id: 101489602504338,
//   theme_color: '#333333',
//   locale: 'en_US'
// })

Vue.config.productionTip = false

/* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   router,
//   components: { App },
//   template: '<App/>'
// })
const gauthOption = {
  clientId: '426863083607-pqaop73jn27dauppd62tloi6bm3hqnpo.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'consent',
  fetch_basic_profile: true
}
Vue.use(GAuth, gauthOption)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
