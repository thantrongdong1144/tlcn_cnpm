/**
 * HTTP client.
 *
 * @see {@link https://github.com/axios/axios}
 * @module utils/axios
 */
import _ from 'lodash'
import axios from 'axios'
import store from '../store'
import notification from './notification'

const config = {
  baseURL: 'http://localhost:13621'
  // baseURL: 'http://trongdong.somee.com'
}

const instance = axios.create(config)

instance.interceptors.request.use(
  function (config) {
    const accessToken = store.state.user.accessToken
    if (!_.isEmpty(accessToken)) {
      config.headers.Authorization = `Bearer ${accessToken}`
    }
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  response => {
    if (response.data.code !== 200) {
      notification.error(response.data.message)
      return Promise.reject(response)
    }
    return response
  },
  error => {
    // Handling error
    console.log('error', error.response)
    if (error.response) {
      let message = ''
      if (error.response.data.message) { message += error.response.data.message + ' ' }
      if (error.response.data.title) { message += error.response.data.title + ' ' }
      if (error.response.data.modelState) { message += error.response.data.modelState.invalid_grant + ' ' }
      if (error.response.data.innerException) { message += error.response.data.innerException.exceptionMessage }
      if (message === '') {
        message = 'Đã có lỗi xảy ra, vui lòng đăng nhập và thử lại'
      }
      notification.error(message)

      return Promise.reject(error)
    }
  }
)

// axios methods
const methods = {
  /**
      * get.
      * @param {String} url Url
      * @param {Object} config Config
      * @return {Promise} Promise
      * @see {@link https://github.com/mzabriskie/axios#instance-methods}
      */
  get (url, config) {
    return instance.get(url, config)
  },

  /**
      * delete.
      * @param {String} url Url
      * @param {Object} config Config
      * @return {Promise} Promise
      * @see {@link https://github.com/mzabriskie/axios#instance-methods}
      */
  delete (url, config) {
    return instance.delete(url, config)
  },

  /**
      * post.
      * @param {String} url Url
      * @param {Object} data Data
      * @param {Object} config Config
      * @return {Promise} Promise
      * @see {@link https://github.com/mzabriskie/axios#instance-methods}
      */
  post (url, data, config) {
    return instance.post(url, data, config)
  },

  /**
      * put.
      * @param {String} url Url
      * @param {Object} data Data
      * @param {Object} config Config
      * @return {Promise} Promise
      * @see {@link https://github.com/mzabriskie/axios#instance-methods}
      */
  put (url, data, config) {
    return instance.put(url, data, config)
  }
}

export default methods
