import Vue from 'vue'
export default {
  success (message) {
    Vue.$toast.success(message, {
      position: 'top-right'
    })
  },
  error (message) {
    Vue.$toast.error(message, {
      position: 'top-right'
    })
  },
  warning (message) {
    Vue.$toast.warning(message, {
      position: 'top-right'
    })
  },
  info (message) {
    Vue.$toast.info(message, {
      position: 'top-right'
    })
  }
}
