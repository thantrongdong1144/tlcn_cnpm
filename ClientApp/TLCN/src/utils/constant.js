export const PaymentType = {
  Cash: 0,
  Momo: 1
}
export const OrderStatus = {
  Processing: 0,
  Approved: 1,
  Delivering: 2,
  Delivered: 3,
  Reject: 4
}
