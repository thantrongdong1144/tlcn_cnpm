import http from '../utils/axios'

export default {
  login (data) {
    return http.post(`/api/auth/login`, data).then(rs => { return rs.data })
  },
  updateUser (data) {
    return http.put(`/api/auth/user/update/${data.id}`, data.user).then(rs => rs.data)
  },
  changePassword (data) {
    return http.put(`/api/auth/user/changePassword/${data.id}`, data.user).then(rs => rs.data)
  },
  forgotPassword (data) {
    return http.post(`/api/auth/forgotpassword`, data).then(rs => rs.data)
  },
  confirmPassword (data) {
    return http.post(`/api/auth/confirmpassword`, data).then(rs => rs.data)
  },
  register (data) {
    return http.post(`/api/auth/register`, data).then(rs => rs.data)
  }
}
