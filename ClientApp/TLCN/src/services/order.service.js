import http from '../utils/axios'

export default {
  addOrder (data) {
    return http.post(`/api/order`, data).then(rs => rs.data)
  },
  addOrderByAdmin (data) {
    return http.post(`/api/order/byadmin`, data).then(rs => rs.data)
  },
  getOrder (pageNumber, pageSize) {
    return http.get(`/api/order?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  getOrderByAdmin (pageNumber, pageSize, search) {
    return http.get(`/api/order/by-admin?PageNumber=${pageNumber}&PageSize=${pageSize}&search=${search}`).then(rs => { return rs.data })
  },
  getOrderByStatus (pageNumber, pageSize, status) {
    return http.get(`/api/order/by-status?PageNumber=${pageNumber}&PageSize=${pageSize}&status=${status}`).then(rs => { return rs.data })
  },
  deleteOrder (id) {
    return http.delete(`/api/order/${id}`).then(rs => rs.data)
  },
  approvedOrder (id) {
    return http.put(`/api/order/approved/${id}`).then(rs => rs.data)
  },
  addOrderDetail (data) {
    return http.post(`/api/order/item`, data).then(rs => rs.data)
  },
  updateOrderDetail (data) {
    return http.put(`/api/order/item`, data).then(rs => rs.data)
  },
  deleteOrderDetail (orderId, productId) {
    return http.delete(`/api/order/item/${orderId}/${productId}`).then(rs => rs.data)
  },
  getListOrderDetails (orderId) {
    return http.get(`/api/order/items/${orderId}`).then(rs => rs.data)
  },
  updateOrder (id, data) {
    return http.put(`/api/order/update/${id}`, data).then(rs => rs.data)
  },
  getListOrderForShipper (shipperId) {
    return http.get(`/api/order/shipper/${shipperId}`).then(rs => rs.data)
  },
  getListOrderForUser (userId) {
    return http.get(`/api/order/user/${userId}`).then(rs => rs.data)
  },
  shipperReceiveOrder (orderId, shipperId) {
    return http.put(`/api/order/shipper/receive/${orderId}/${shipperId}`).then(rs => rs.data)
  },
  shipperCompleteOrder (orderId) {
    return http.put(`/api/order/shipper/complete/${orderId}`).then(rs => rs.data)
  },
  shipperRejectOrder (orderId, note) {
    return http.put(`/api/order/shipper/reject/${orderId}/${note}`).then(rs => rs.data)
  },
  userRejectOrder (orderId, note) {
    return http.put(`/api/order/user/reject/${orderId}/${note}`).then(rs => rs.data)
  },
  updateAcceptPayment (id) {
    return http.put(`/api/order/accept-payment/${id}`).then(rs => rs.data)
  }
}
