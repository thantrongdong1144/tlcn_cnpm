import http from '../utils/axios'

export default {
  getMenu (pageNumber, pageSize) {
    return http.get(`/api/menu?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  getMenuDetails (id) {
    return http.get(`/api/menu/items/${id}`).then(rs => { return rs.data })
  }
}
