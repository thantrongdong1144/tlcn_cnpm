import http from '../utils/axios'
export default {
  getListFavoriteProduct (id) {
    return http.get(`/api/favoriteproduct/user/${id}`).then(rs => rs.data)
  },
  likeOrUnlikeProduct (userId, productId) {
    return http.post(`/api/favoriteproduct/like/${userId}/${productId}`).then(rs => rs.data)
  }
}
