import http from '../utils/axios'

export default {
  getRevenueData () {
    return http.get(`/api/dashboard/chart/revenue`).then(rs => { return rs.data })
  },
  getRejectTotalData () {
    return http.get(`/api/dashboard/chart/rejecttotal`).then(rs => { return rs.data })
  },
  getNumOfOrderByMonth () {
    return http.get(`/api/dashboard/chart/num_order_in_month`).then(rs => { return rs.data })
  },
  getProductTotalOrderByCategory () {
    return http.get(`/api/dashboard/chart/total_product_order_by_category`).then(rs => { return rs.data })
  },
  getParameterTotalOrder () {
    return http.get(`/api/dashboard/parameter/total_order`).then(rs => { return rs.data })
  },
  getParameterTotalRevenue () {
    return http.get(`/api/dashboard/parameter/total_revenue`).then(rs => { return rs.data })
  },
  getParameterTotalUser () {
    return http.get(`/api/dashboard/parameter/total_user`).then(rs => { return rs.data })
  },
  getParameterTotalRejectOrder () {
    return http.get(`/api/dashboard/parameter/total_reject_order`).then(rs => { return rs.data })
  }
}
