import http from '../utils/axios'

export default {
  getCategories (pageNumber, pageSize) {
    return http.get(`/api/category?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  getCategoriesByAdmin (pageNumber, pageSize, search) {
    return http.get(`/api/category/by-admin?PageNumber=${pageNumber}&PageSize=${pageSize}&search=${search}`).then(rs => { return rs.data })
  },
  getCategorieWithQuantity () {
    return http.get(`/api/category/withquantity`).then(rs => { return rs.data })
  },
  getCategoryById (id) {
    return http.get(`/api/category/${id}`).then(rs => rs.data.data)
  },
  addCategory (data) {
    return http.post(`/api/category`, data).then(rs => rs.data)
  },
  updateCategory (data) {
    return http.put(`/api/category/${data.id}`, data.category).then(rs => rs.data)
  },
  deleteCategory (id) {
    return http.delete(`/api/category/${id}`).then(rs => rs.data)
  }
}
