import http from '../utils/axios'

export default {
  getProducts (pageNumber, pageSize) {
    return http.get(`/api/product?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  getProductHasDiscounts (pageNumber, pageSize, isDiscount) {
    return http.get(`/api/product/hasDiscount?PageNumber=${pageNumber}&PageSize=${pageSize}&isDiscount=${isDiscount}`).then(rs => { return rs.data })
  },
  getProductsByAdmin (pageNumber, pageSize, search) {
    return http.get(`/api/product/by-admin?PageNumber=${pageNumber}&PageSize=${pageSize}&search=${search}`).then(rs => { return rs.data })
  },
  getNewProduct () {
    return http.get(`/api/product/new`).then(rs => { return rs.data })
  },
  getPopularProduct () {
    return http.get(`/api/product/popular`).then(rs => { return rs.data })
  },
  getPopularProductLastWeek () {
    return http.get(`/api/product/popularlastweek`).then(rs => { return rs.data })
  },
  getProductById (id) {
    return http.get(`/api/product/${id}`).then(rs => rs.data.data)
  },
  checkOnSale (id) {
    return http.get(`/api/Product/check-on-sale/${id}`).then(rs => rs.data.data)
  },
  getProductsByCategory (id, pageNumber, pageSize) {
    return http.get(`/api/product/search/category/${id}?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  getProductsByProductKey (key, pageNumber, pageSize) {
    return http.get(`/api/product/search/product/${key}?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  addProduct (data) {
    return http.post(`/api/product`, data).then(rs => rs.data)
  },
  addDiscountProduct (data) {
    return http.post(`/api/product/discount`, data).then(rs => rs.data)
  },
  updateProduct (data) {
    return http.put(`/api/product/${data.id}`, data.product).then(rs => rs.data)
  },
  deleteProduct (id) {
    return http.delete(`/api/product/${id}`).then(rs => rs.data)
  }
}
