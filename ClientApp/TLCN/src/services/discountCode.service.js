import http from '../utils/axios'

export default {
  getListDiscountCode (id) {
    return http.get(`/api/discountcode/user/${id}`).then(rs => rs.data)
  },
  checkCode (code) {
    return http.get(`/api/discountcode/check/${code}`).then(rs => rs)
  },
  useCode (code) {
    return http.put(`/api/discountcode/use/${code}`).then(rs => rs.data)
  },
  getDiscountCodes (pageNumber, pageSize) {
    return http.get(`/api/discountcode?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  getDiscountCodeById (id) {
    return http.get(`/api/discountcode/${id}`).then(rs => rs.data.data)
  },
  addDiscountCode (data) {
    return http.post(`/api/discountcode`, data).then(rs => rs.data)
  },
  updateDiscountCode (data) {
    return http.put(`/api/DiscountCode/${data.id}`, data.code).then(rs => rs.data)
  },
  deleteDiscountCode (id) {
    return http.delete(`/api/DiscountCode/${id}`).then(rs => rs.data)
  }
}
