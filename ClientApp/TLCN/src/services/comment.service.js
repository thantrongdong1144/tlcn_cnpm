import http from '../utils/axios'

export default {
  addComment (data) {
    return http.post(`/api/comment`, data).then(rs => rs.data)
  },
  getCommentByProductId (id, pageNum, pageSize) {
    return http.get(`/api/comment/product/${id}?PageNumber=${pageNum}&PageSize=${pageSize}`).then(rs => rs.data)
  },
  getComments (pageNumber, pageSize) {
    return http.get(`/api/comment?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  getCommentById (id) {
    return http.get(`/api/comment/${id}`).then(rs => rs.data.data)
  },
  addCommentbyAdmin (data) {
    return http.post(`/api/comment/admin`, data).then(rs => rs.data)
  },
  updateComment (data) {
    return http.put(`/api/comment/${data.id}`, data.comment).then(rs => rs.data)
  },
  deleteComment (id) {
    return http.delete(`/api/comment/${id}`).then(rs => rs.data)
  }
}
