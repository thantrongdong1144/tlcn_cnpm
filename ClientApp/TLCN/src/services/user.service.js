import http from '../utils/axios'

export default {
  getUsers (pageNumber, pageSize) {
    return http.get(`/api/user?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  getShippers (pageNumber, pageSize) {
    return http.get(`/api/user/shipper?PageNumber=${pageNumber}&PageSize=${pageSize}`).then(rs => { return rs.data })
  },
  getUsersByAdmin (pageNumber, pageSize, search) {
    return http.get(`/api/user/by-admin?PageNumber=${pageNumber}&PageSize=${pageSize}&search=${search}`).then(rs => { return rs.data })
  },
  getUserById (id) {
    return http.get(`/api/user/${id}`).then(rs => rs.data.data)
  },
  addUser (data) {
    return http.post(`/api/user`, data).then(rs => rs.data)
  },
  updateUser (data) {
    return http.put(`/api/user/${data.id}`, data.user).then(rs => rs.data)
  },
  deleteUser (id) {
    return http.delete(`/api/user/${id}`).then(rs => rs.data)
  }
}
