import favoriteProductService from '../services/favoriteProduct.service'
export default {
  namespaced: true,
  state: {
    favoriteProduct: []
  },
  actions: {
    async getProduct ({commit}, data) {
      var result = await favoriteProductService.getListFavoriteProduct(data)
      if (result) {
        commit('setProduct', result.data)
        return result
      }
      return null
    },
    async likeOrUnlike ({commit}, data) {
      var result = await favoriteProductService.likeOrUnlikeProduct(data.userId, data.productId)
      if (result.code === 200) {
        var result2 = await favoriteProductService.getListFavoriteProduct(data.userId)
        if (result2) {
          commit('setProduct', result2.data)
          return result
        }
        return result
      }
      return null
    }
  },
  getters: {
    favoriteProduct: (state) => {
      return state.favoriteProduct
    }
  },
  mutations: {
    setProduct: (state, product) => {
      state.favoriteProduct = product
    }
  }
}
