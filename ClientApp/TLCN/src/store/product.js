import productService from '../services/product.service'

export default {
  namespaced: true,
  state: {
    allProducts: {},
    product: {},
    newProducts: [],
    popularProducts: [],
    popularLastWeekProduct: []
  },
  actions: {
    async loadAllProducts ({ commit }) {
      const result = await productService.getProducts(1, 99999)
      if (result) {
        commit('setAllProducts', result)
        return result
      }
      return null
    },
    async loadNewProduct ({ commit }) {
      const result = await productService.getNewProduct()
      if (result) {
        commit('setNewProduct', result.data)
        return result
      }
      return null
    },
    async loadPopularProduct ({ commit }) {
      const result = await productService.getPopularProduct()
      if (result) {
        commit('setPopularProduct', result.data)
        return result
      }
      return null
    },
    async loadPopularLastWeekProduct ({ commit }) {
      const result = await productService.getPopularProductLastWeek()
      if (result) {
        commit('setPopularLastWeekProduct', result.data)
        return result
      }
      return null
    },
    async getProducts ({ commit }, data) {
      const result = await productService.getProducts(data.pageNumber, data.pageSize)
      if (result) {
        commit('setProducts', result)
        return result
      }
      return null
    },
    async getProductsByCategory ({ commit }, data) {
      const result = await productService.getProductsByCategory(data.id, data.pageNumber, data.pageSize)
      if (result) {
        commit('setProducts', result)
        return result
      }
      return null
    },
    async getProductsByProductKey ({ commit }, data) {
      const result = await productService.getProductsByProductKey(data.key, data.pageNumber, data.pageSize)
      if (result) {
        commit('setProducts', result)
        return result
      }
      return null
    }
  },
  getters: {
    getAllProducts: (state) => {
      return state.allProducts
    },
    getProducts: (state) => {
      return state.product
    },
    newProducts: (state) => {
      return state.newProducts
    },
    popularProducts: (state) => {
      return state.popularProducts
    },
    popularLastWeekProduct: (state) => {
      return state.popularLastWeekProduct
    }
  },
  mutations: {
    setAllProducts: (state, allProducts) => {
      state.allProducts = allProducts
    },
    setProducts: (state, product) => {
      state.product = product
    },
    setNewProduct: (state, product) => {
      state.newProducts = product
    },
    setPopularProduct: (state, product) => {
      state.popularProducts = product
    },
    setPopularLastWeekProduct: (state, product) => {
      state.popularLastWeekProduct = product
    }
  }
}
