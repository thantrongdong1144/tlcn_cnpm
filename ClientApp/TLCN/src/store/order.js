import orderService from '../services/order.service'

export default {
  namespaced: true,
  state: {
    listOrderForShipper: {
      'data': []
    },
    listOrderForUser: {
      'data': []
    }
  },
  actions: {
    async getListOrderForShipper ({ commit }, data) {
      const result = await orderService.getListOrderForShipper(data)
      if (result) {
        commit('setListOrderForShipper', result)
        return result
      }
      return null
    },
    async getListOrderForUser ({ commit }, data) {
      const result = await orderService.getListOrderForUser(data)
      if (result) {
        commit('setListOrderForUser', result)
        return result
      }
      return null
    }
  },
  getters: {
    listOrderForShipper: (state) => {
      return state.listOrderForShipper
    },
    listOrderForUser: (state) => {
      return state.listOrderForUser
    }
  },
  mutations: {
    setListOrderForShipper: (state, orders) => {
      state.listOrderForShipper = orders
    },
    setListOrderForUser: (state, orders) => {
      state.listOrderForUser = orders
    }
  }
}
