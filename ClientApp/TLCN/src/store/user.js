import authService from '../services/auth.service'
import userService from '../services/user.service'
export default {
  namespaced: true,
  state: {
    user: {},
    refreshEmail: '',
    allUser: [],
    accessToken: ''
  },
  actions: {
    async loadAllUsers ({ commit }) {
      const result = await userService.getUsers(1, 99999)
      if (result) {
        commit('setAllUsers', result)
        return result
      }
      return null
    },
    async login ({commit}, data) {
      const rs = await authService.login(data)
      if (rs && rs.code === 200) {
        commit('setUser', rs.data)
        commit('setAccessToken', rs.token)
        return rs
      }
      return null
    },
    logout ({commit}, data) {
      commit('setUser', {})
    },
    setUser ({commit}, user) {
      commit('setUser', user)
    },
    setRefreshEmail ({commit}, email) {
      commit('setRefreshEmail', email)
    },
    setAccessToken ({commit}, token) {
      commit('setAccessToken', token)
    }
  },
  getters: {
    currentUser: (state) => {
      return state.user
    },
    getRefreshEmail: (state) => {
      return state.refreshEmail
    },
    allUser: (state) => {
      return state.allUser
    },
    accessToken: (state) => {
      return state.accessToken
    }
  },
  mutations: {
    setUser: (state, user) => {
      state.user = user
    },
    setRefreshEmail: (state, email) => {
      state.refreshEmail = email
    },
    setAllUsers: (state, users) => {
      state.allUser = users
    },
    setAccessToken: (state, accessToken) => {
      state.accessToken = accessToken
    }
  }
}
