import notification from '../utils/notification'
export default {
  namespaced: true,
  state: {
    productComputed: []
  },
  actions: {
    addProductComputed ({commit}, product) {
      if (product.id) {
        commit('addProductComputed', product)
      }
    },
    deleteProductComputed ({commit}, productId) {
      commit('deleteProductComputed', productId)
    },
    clearProductComputed ({commit}) {
      commit('clearProductComputed')
    }

  },
  getters: {
    productComputed: (state) => {
      return state.productComputed
    }
  },
  mutations: {
    addProductComputed: (state, product) => {
      if (state.productComputed.findIndex(p => p.id === product.id) !== -1) {
        state.productComputed = state.productComputed.map(p => {
          if (p.id === product.id) {
            p.quantity += product.quantity
          }
          return p
        })
      } else {
        state.productComputed.push({
          id: product.id,
          name: product.name,
          code: product.code,
          picture: product.picture,
          price: product.price,
          quantity: product.quantity,
          rate: product.star
        })
      }
      notification.success('Đã thêm vào danh sách so sánh')
    },
    deleteProductComputed: (state, productId) => {
      state.productComputed = state.productComputed.filter(p => p.id !== productId)
    },
    clearProductComputed: (state) => {
      state.productComputed = []
    }
  }
}
