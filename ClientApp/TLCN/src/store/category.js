import categoryService from '../services/category.service'

export default {
  namespaced: true,
  state: {
    listCategory: [],
    listCategoryWithQuantity: [],
    category: {}
  },
  actions: {
    async getCategories ({ commit }, data) {
      const result = await categoryService.getCategories(data.pageNumber, data.pageSize)
      if (result) {
        commit('setCategories', result)
        return result
      }
      return null
    },
    async getCategoriesWithQuantity ({ commit }) {
      const result = await categoryService.getCategorieWithQuantity()
      console.log('result', result)
      if (result) {
        commit('setCategoryWithQuantity', result)
        return result
      }
      return null
    }

  },
  getters: {
    getCategories: (state) => {
      return state.listCategory
    },
    listCategoryWithQuantity: (state) => {
      return state.listCategoryWithQuantity
    }
  },
  mutations: {
    setCategories: (state, listCategory) => {
      state.listCategory = listCategory
    },
    setCategoryWithQuantity: (state, listCategory) => {
      state.listCategoryWithQuantity = listCategory
    }
  }
}
