import notification from '../utils/notification'
import productService from '../services/product.service'
import { stringUtil } from '../utils/string'
export default {
  namespaced: true,
  state: {
    cart: []
  },
  /*
    cart item = {
        id: number
        name : string,
        code : string,
        picture: string,
        price: number,
        quantity: number
    }
  */
  actions: {
    addProduct ({commit}, product) {
      if (product.id) {
        commit('addProduct', product)
      }
    },
    deleteproduct ({commit}, productId) {
      commit('deleteproduct', productId)
    },
    clearCart ({commit}) {
      commit('clearCart')
    }

  },
  getters: {
    cart: (state) => {
      return state.cart
    }
  },
  mutations: {
    addProduct: async (state, product) => {
      const data = await productService.checkOnSale(product.id)
      if (data.isDeleted) {
        notification.error('Sản phẩm đã ngừng kinh doanh. Quý khách vui lòng lựa chọn sản phẩm khác')
        return
      }
      let price = product.price
      console.log(' this.distanceBetweenToDate(product.expirationDiscountDate) >= 0', product, stringUtil.distanceBetweenToDate(product.expirationDiscountDate))
      if (product.expirationDiscountDate && product.discountPrice && stringUtil.distanceBetweenToDate(product.expirationDiscountDate) >= 0) {
        price = product.discountPrice
      }
      if (state.cart.findIndex(p => p.id === product.id) !== -1) {
        state.cart = state.cart.map(p => {
          if (p.id === product.id) {
            p.quantity += product.quantity
          }
          return p
        })
      } else {
        state.cart.push({
          id: product.id,
          name: product.name,
          picture: product.picture,
          price: price,
          quantity: product.quantity
        })
      }
      notification.success('Đã thêm vào giỏ hàng')
    },
    deleteproduct: (state, productId) => {
      state.cart = state.cart.filter(p => p.id !== productId)
    },
    clearCart: (state) => {
      state.cart = []
    }
  }
}
