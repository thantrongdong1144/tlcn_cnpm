import discountCodeService from '../services/discountCode.service'
export default {
  namespaced: true,
  state: {
    listDiscountCode: []
  },
  actions: {
    async getListDiscountCode ({ commit }, data) {
      const result = await discountCodeService.getListDiscountCode(data)
      if (result) {
        commit('setListDiscountCode', result)
        return result
      }
      return null
    }
  },
  getters: {
    listDiscountCode: (state) => {
      return state.listDiscountCode
    }
  },
  mutations: {
    setListDiscountCode: (state, discount) => {
      state.listDiscountCode = discount
    }
  }
}
