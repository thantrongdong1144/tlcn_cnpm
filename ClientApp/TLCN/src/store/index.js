import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import productModule from './product'
import cartModule from './cart'
import userModule from './user'
import categoryModule from './category'
import discountCodeModule from './discountCode'
import favoriteProductModule from './favoriteProduct'
import productComputedModule from './productComputed'
import orderModule from './order'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    appName: 'Chợ',
    newKey: 'asfdssskdskkfskjdfjksjkjnngrtarrw'
  },
  mutations: {
  },
  actions: {
  },
  getters: {
    appName: state => { return state.appName },
    newKey: state => { return state.newKey }
  },
  modules: {
    product: productModule,
    cart: cartModule,
    user: userModule,
    category: categoryModule,
    discountCode: discountCodeModule,
    favoriteProduct: favoriteProductModule,
    productComputed: productComputedModule,
    order: orderModule
  },
  plugins: [createPersistedState()]
})
